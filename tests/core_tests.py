import unittest
from datetime import (datetime,
                      timedelta)

from tm_core import exeptions

#from . import config

from tm_core.control.core_interface import CoreInterface
from tm_core import exeptions


class CoreTest(unittest.TestCase):

    def test_user_show_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'
        CoreInterface.get_users_info()

    def test_user_add_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'

        CoreInterface.user_add('TestLogin1', 'TestFirstName', 'TestLastName', 'TestEmail@mail.com', 'TestPassword')

        users_info = CoreInterface.get_users_info()
        new_user_exists = False
        for user_info in users_info:
            if user_info['login'] == 'TestLogin1':
                new_user_exists = True
                break
        self.assertTrue(new_user_exists, 'User wasn\'t added.')

        CoreInterface.user_add('TestLogin2')

        users_info = CoreInterface.get_users_info()
        new_user_exists = False
        for user_info in users_info:
            if user_info['login'] == 'TestLogin2':
                new_user_exists = True
                break
        self.assertTrue(new_user_exists, 'User wasn\'t added.')

        try:
            CoreInterface.user_add('123')
            self.fail('User with bad login was added.')
        except exeptions.InvalidNameException:
            pass

        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin1'))
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin2'))

    def test_user_remove_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'

        CoreInterface.user_add('TestLogin3')

        CoreInterface.user_add('TestLogin2')
        try:
            CoreInterface.user_remove(CoreInterface.get_user_id('TestLogin2'), CoreInterface.get_user_id('TestLogin3'))
            self.fail('User was deleted by user without rights.')
        except exeptions.WrongPasswordException:
            pass
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin2'))

        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin3'))

        users_info = CoreInterface.get_users_info()
        removed_user_exists = False
        for user_info in users_info:
            if user_info['login'] == 'TestLogin3':
                removed_user_exists = True
                break
        self.assertFalse(removed_user_exists, 'User wasn\'t removed.')

    def test_user_edit_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'
        CoreInterface.user_add('TestLogin1')
        CoreInterface.user_edit(0, CoreInterface.get_user_id('TestLogin1'), 'TestLogin2', 'AnotherTestFirstName',
                                'AnotherTestLastName', 'AnotherTestEmail@mail.com', 'AnotherTestPassword', None)

        users_info = CoreInterface.get_users_info()
        changed_user_exists = False
        for user_info in users_info:
            if user_info['login'] == 'TestLogin2':
                changed_user_exists = True
                break
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin2'))

        self.assertTrue(changed_user_exists, 'User wasn\'t changed.')

    def test_task_show_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'
        CoreInterface.user_add('TestLogin1')
        CoreInterface.get_tasks_info(CoreInterface.get_user_id('TestLogin1'))
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin1'))


    def test_task_add_operation(self):
        CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'
        CoreInterface.user_add('TestLogin1')
        CoreInterface.task_add(0, (CoreInterface.get_user_id('TestLogin1')), 'TestName1', 'TestDescription',
                               datetime.now(), datetime.now() + timedelta(days=1))

        tasks_info = CoreInterface.get_tasks_info(CoreInterface.get_user_id('TestLogin1'))
        new_task_exists = False
        for task_info in tasks_info:
            if task_info['name'] == 'TestName1':
                new_task_exists = True
                break
        CoreInterface.task_remove(0, CoreInterface.get_my_task_id_by_name(CoreInterface.get_user_id('TestLogin1'),
                                                                          'TestName1'))

        self.assertTrue(new_task_exists, 'Task wasn\'t added.')

        CoreInterface.task_add(0, (CoreInterface.get_user_id('TestLogin1')), 'TestName2')

        tasks_info = CoreInterface.get_tasks_info(CoreInterface.get_user_id('TestLogin1'))
        new_task_exists = False
        for task_info in tasks_info:
            if task_info['name'] == 'TestName2':
                new_task_exists = True
                break
        CoreInterface.task_remove(0, CoreInterface.get_my_task_id_by_name(CoreInterface.get_user_id('TestLogin1'),
                                                                          'TestName2'))
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin1'))

        self.assertTrue(new_task_exists, 'Task wasn\'t added.')

    def test_task_remove_operation(self):
        CoreInterface.user_add('TestLogin1')
        user_id = CoreInterface.get_user_id('TestLogin1')
        CoreInterface.task_add(0, user_id, 'TestName1')
        task_id = CoreInterface.get_my_task_id_by_name(user_id, 'TestName1')

        CoreInterface.user_add('TestLogin2')
        try:
            CoreInterface.task_remove(CoreInterface.get_user_id('TestLogin2'), task_id)
            self.fail('Task was deleted by user without rights.')
        except exeptions.NotEnoughRightsException:
            pass
        CoreInterface.user_remove(0, CoreInterface.get_user_id('TestLogin2'))

        CoreInterface.task_remove(0, task_id)

        try:
            CoreInterface.get_my_task_id_by_name(user_id, 'TestName1')
            self.fail('Task wasn\'t removed.')
        except exeptions.TaskMissingException:
            pass

        CoreInterface.user_remove(0, user_id)

    def test_task_edit_operation(self):
        CoreInterface.user_add('TestLogin1')
        user_id = CoreInterface.get_user_id('TestLogin1')
        CoreInterface.task_add(0, user_id, 'TestName1')
        task_id = CoreInterface.get_my_task_id_by_name(user_id, 'TestName1')
        CoreInterface.task_edit(0, task_id, 'NewName', 'NewDescription')

        try:
            CoreInterface.get_my_task_id_by_name(user_id, 'TestName1')
            self.fail('Task wasn\'t edited.')
        except exeptions.TaskMissingException:
            pass

        CoreInterface.task_remove(0, task_id)
        CoreInterface.user_remove(0, user_id)


if __name__ == '__main__':
    CoreInterface.db_address = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'
    users_info = CoreInterface.get_users_info()

    can_make_test = True
    for user_info in users_info:
        if user_info['login'] == 'TestLogin1' or user_info['login'] == 'TestLogin2' or user_info['login'] == 'TestLogin3':
            can_make_test = False
            break

    if can_make_test:
        unittest.main()
