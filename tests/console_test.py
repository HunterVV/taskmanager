import unittest

from . import config

from tm_console.tm_interpreter import Parser

from tm_core.control.core_interface import CoreInterface
from tm_core import exeptions


class ControllerTest(unittest.TestCase):

    def test_log_in_out_operation(self):
        try:
            exec('-m tm_console user add TestUser -p=123456')
            exec('-m tm_console login TestUser -p=123456')
        except exeptions.UserAlreadyExists:
            pass

    def test_log_out_operation(self):
        exec('-m tm_console logout')


if __name__ == '__main__':
    unittest.main()
