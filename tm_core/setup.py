from setuptools import setup, find_packages

setup(
    name='tm_core',
    version='1.0.0',
    author='Vitaly Vasilyeu',
    author_email='vasilvvmain@gmail.com',
    description='Core library for task manager applications.',
    packages=find_packages(),
    install_requires=['sqlalchemy'],
)
