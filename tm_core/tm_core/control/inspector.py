from datetime import datetime

from ..exeptions import (InvalidNameException,
                         TimeLogicException,
                         NotEnoughRightsException,
                         WrongPasswordException,
                         UserMissingException,
                         UserAlreadyExistsException,
                         ScriptLogicException)


class Inspector:

    # Checking names
    @staticmethod
    def check_name(input_string):
        """
        Function for checking if string can be a name. It will throw exception, if not.
        :param input_string: String to check
        :return: None
        """
        if Inspector.is_int(input_string) or input_string is None:
            raise InvalidNameException('The proposed name can\'t be used.')

    # Check email
    @staticmethod
    def check_email(input_string):
        """
        Function for checking if string can be an email. It will throw exception, if not.
        :param input_string: String to check
        :return: None
        """
        if not (Inspector.is_email(input_string)):
            raise InvalidNameException('Entered invalid email.')

    # Check password
    @staticmethod
    def check_password(input_string):
        """
        Function for checking if string can be a password. It will throw exception, if not.
        :param input_string: String to check
        :return: None
        """
        if len(input_string) > 24 or len(input_string) < 4:
            raise InvalidNameException('Entered password can\'t be used.')

    # Is integer?
    @staticmethod
    def is_int(input_string):
        """
        Function for checking if string is integer.
        :param input_string: String to check
        :return: Verdict
        """
        try:
            int(input_string)
            return True
        except ValueError:
            return False

    # Is email?
    @staticmethod
    def is_email(input_string):
        """
        Function for checking if string is a mail.
        :param input_string:
        :return: Verdict
        """
        return True

    # Method for checking time logic
    @staticmethod
    def check_datetime_logic(input_start_datetime, input_end_datetime):
        """
        Function for checking time logic. Start datetime must be >= now and < end datetime. It will throw exception, if
        time logic was broken.
        :param input_start_datetime: Start datetime
        :param input_end_datetime: End datetime
        :return: None
        """
        if input_end_datetime is not None:
            if input_start_datetime is not None:
                if input_start_datetime >= input_end_datetime:
                    raise TimeLogicException('Time logic was broken.')
            elif datetime.now() > input_end_datetime:
                raise TimeLogicException('Time logic was broken.')

    # Method for checking user rights
    @staticmethod
    def check_user_rights(input_user_id, input_task_id, input_db_worker):
        """
        Function for checking user's right to work with task.  It will throw exception, if executor doesn't have enough
        rights.
        :param input_user_id: User's id
        :param input_task_id: Tasks's id
        :param input_db_worker: Database worker
        :return: Verdict
        """
        user_task_relationship = input_db_worker.get_user_task_relationship(input_user_id, input_task_id)
        if user_task_relationship.user_rights < 1:
            raise NotEnoughRightsException('Not enough rights.')

# ======================================================================================================================
# Operations check section

    # Add user operation check
    @staticmethod
    def user_add_check(input_login, input_email, input_password, input_db_worker):
        Inspector.check_name(input_login)

        possible_user = None
        try:
            possible_user = input_db_worker.get_user_by_login(input_login)
        except UserMissingException:
            pass
        if possible_user is not None:
            raise UserAlreadyExistsException('User with this login already exists.')

        if input_email is not None:
            Inspector.check_email(input_email)

        if input_password is not None and input_password != '':
            Inspector.check_password(input_password)

    # Edit user operation check
    @staticmethod
    def user_edit_check(input_executor_id, input_user_id, input_login, input_email, input_new_password,
                        input_old_password, input_db_worker):
        user = input_db_worker.get_user_by_id(input_user_id)

        if input_user_id != input_executor_id and input_executor_id != 0:
            if user.password != input_old_password:
                raise WrongPasswordException('Entered password is wrong.')

        possible_user = None
        if input_login is not None:
            if user.login != input_login:
                try:
                    possible_user = input_db_worker.get_user_by_login(input_login)
                except UserMissingException:
                    pass
                if possible_user is not None:
                    raise UserAlreadyExistsException('User with this login already exists.')
            Inspector.check_name(input_login)

        if input_email is not None:
            Inspector.check_email(input_email)

        if input_new_password is not None and input_new_password != '':
            Inspector.check_password(input_new_password)

    # Remove user operation check
    @staticmethod
    def user_remove_check(input_executor_id, input_user_id, input_password, input_db_worker):
        user = input_db_worker.get_user_by_id(input_user_id)

        if input_executor_id != 0:
            if user.password != input_password:
                raise WrongPasswordException('Entered password is wrong.')

    # Add task operation check
    @staticmethod
    def task_add_check(input_executor_id, input_user_id, input_name, input_start_datetime, input_end_datetime):
        if input_user_id != input_executor_id and input_executor_id != 0:
            raise NotEnoughRightsException('Not enough right to create task.')

        Inspector.check_name(input_name)

        Inspector.check_datetime_logic(input_start_datetime, input_end_datetime)

    # Edit task operation check
    @staticmethod
    def task_edit_check(input_executor_id, input_task_id, input_name, input_start_datetime, input_end_datetime,
                        input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            user = input_db_worker.get_user_by_id(input_executor_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough right to create task.')
            else:
                Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

        if input_name is not None:
            Inspector.check_name(input_name)

        Inspector.check_datetime_logic(input_start_datetime, input_end_datetime)

    # Remove task operation check
    @staticmethod
    def task_remove_check(input_executor_id, input_task_id, input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            user = input_db_worker.get_user_by_id(input_executor_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough right to create task.')
            else:
                Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

    # Change status operation check
    @staticmethod
    def status_change_check(input_executor_id, input_task_id, input_status_name, input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            user = input_db_worker.get_user_by_id(input_executor_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough right to create task.')
            else:
                Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

        input_db_worker.get_status(input_status_name)

    # Add subuser operation check
    @staticmethod
    def subuser_add_check(input_executor_id, input_task_id, input_subuser_id, input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            user = input_db_worker.get_user_by_id(input_executor_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough right to create task.')
            else:
                Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

        input_db_worker.get_user_by_id(input_subuser_id)

    # Remove subuser operation check
    @staticmethod
    def subuser_remove_check(input_executor_id, input_task_id, input_subuser_id, input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            user = input_db_worker.get_user_by_id(input_executor_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough right to create task.')
            elif input_executor_id == input_subuser_id:
                pass
            else:
                Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

        if input_subuser_id != 0:
            input_db_worker.get_user_by_id(input_subuser_id)

    # Add subtask operation check
    @staticmethod
    def subtask_add_check(input_executor_id, input_ownertask_id, input_subtask_id, input_db_worker):
        ownertask = input_db_worker.get_task(input_ownertask_id)
        subtask = input_db_worker.get_task(input_subtask_id)

        if len(ownertask.script) != 0 or len(subtask.script) != 0:
            raise ScriptLogicException('Tasks with scripts can\'t have subtasks or ownertask.')

        if input_executor_id != 0:
            input_db_worker.get_user_by_id(input_executor_id)
            Inspector.check_user_rights(input_executor_id, input_ownertask_id, input_db_worker)
            Inspector.check_user_rights(input_executor_id, input_subtask_id, input_db_worker)

    # Remove subtask operation check
    @staticmethod
    def subtask_remove_check(input_executor_id, input_ownertask_id, input_subtask_id, input_db_worker):
        input_db_worker.get_task(input_ownertask_id)
        if input_subtask_id != 0:
            input_db_worker.get_task(input_subtask_id)

        if input_executor_id != 0:
            input_db_worker.get_user_by_id(input_executor_id)
            Inspector.check_user_rights(input_executor_id, input_ownertask_id, input_db_worker)

    # Change script operation check
    @staticmethod
    def script_change_check(input_executor_id, input_task_id, input_task_name, input_add_statuses,
                            input_remove_statuses, input_db_worker):
        task = input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)

        if input_db_worker.get_subtasks(input_task_id) is not None and task.ownertask_id is not None:
            raise ScriptLogicException('Task with subtasks or ownertask can\'t have script.')

        if input_task_name is not None:
            Inspector.check_name(input_task_name)

        if input_add_statuses is not None:
            for status_name in input_add_statuses:
                input_db_worker.get_status(status_name)

        if input_remove_statuses is not None:
            for status_name in input_remove_statuses:
                input_db_worker.get_status(status_name)

    # Remove script operation check
    @staticmethod
    def script_remove_check(input_executor_id, input_task_id, input_db_worker):
        input_db_worker.get_task(input_task_id)

        if input_executor_id != 0:
            Inspector.check_user_rights(input_executor_id, input_task_id, input_db_worker)
