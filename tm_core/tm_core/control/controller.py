import logging

from datetime import (datetime,
                      timedelta)

from .inspector import Inspector
from ..models.entities import (User,
                               Task,
                               Script)
from ..models.db_worker import DatabaseWorker
from ..exeptions import (UncertaintyException,
                         TaskMissingException,
                         NotEnoughRightsException,
                         TimeLogicException,
                         WrongPasswordException)


class Controller:
    def __init__(self, input_db_address, input_user_id=None):
        self.logger = logging.getLogger('coreLogger.' + __name__)
        self.logger.debug('Creating Controller.')
        self.db_worker = DatabaseWorker(input_db_address)
        self.executor_user_id = input_user_id

    def __del__(self):
        del self.db_worker

    db_address = None

# ======================================================================================================================
# Basic operations section

    # Method for getting user id by login
    def get_user_id(self, input_login):
        self.logger.debug('Trying to get user\'s id by login={user_login}.'.format(user_login=input_login))
        user = self.db_worker.get_user_by_login(input_login)

        self.logger.debug('Returning id of user with login={user_login}.'.format(user_login=input_login))
        return user.id

    # Method for getting user login by id
    def get_user_login(self, input_user_id):
        self.logger.debug('Trying to get user\'s login by id={user_id}.'.format(user_id=input_user_id))
        user = self.db_worker.get_user_by_id(input_user_id)

        self.logger.debug('Returning login of user with id={user_id}.'.format(user_id=input_user_id))
        return user.login

    # Method for getting user's task id by name
    def get_executor_task_id(self, input_task_name):
        self.logger.debug('Trying to get executor\'s (id={user_id}) task\'s id by name={task_name}.'.
                          format(user_id=self.executor_user_id, task_name=input_task_name))
        user = self.db_worker.get_user_by_id(self.executor_user_id)
        tasks = self.db_worker.get_all_tasks()

        final_tasks_list = []
        for task in tasks:
            if task.name == input_task_name and user in task.users:
                final_tasks_list.append(task)

        if final_tasks_list.__len__() > 1:
            self.logger.debug('User with id={user_id} have more than one task with name={task_name}.'.
                              format(user_id=self.executor_user_id, task_name=input_task_name))
            raise UncertaintyException('There more than 1 tasks with given name.')
        if final_tasks_list.__len__() == 0:
            self.logger.debug('User with id={user_id} doesn\'t have task with name={task_name}.'.
                              format(user_id=self.executor_user_id, task_name=input_task_name))
            raise TaskMissingException('Task with this name wasn\'t found')

        self.logger.debug('Returning task id with name={task_name}.'.format(task_name=input_task_name))
        return final_tasks_list[0].id

    # Method for getting task's status name
    def get_tasks_status_name(self, input_task_id):
        self.logger.debug('Trying to get task\'s (id={task_id}) status name.'.format(task_id=input_task_id))
        user = self.db_worker.get_user_by_id(self.executor_user_id)
        task = self.db_worker.get_task(input_task_id)

        self.logger.debug('Checking rights for get task\'s status operation.')
        if user not in task.users or self.executor_user_id != 0:
            self.logger.debug('Exeutor with id={user_id} doesn\'t have enough rigths to get status name.'.
                              format(user_id=self.executor_user_id))
            raise NotEnoughRightsException('Not enough rights to see task\'s status.')

        self.logger.debug('Returning task\'s (id={task_id}) status name'.format(task_id=input_task_id))
        return task.status.status

    # Method for cheking user's password
    def check_users_password(self, input_user_id, input_password):
        self.logger.debug('Trying to check password of user with id={user_id}.'.format(user_id=input_user_id))
        user = self.db_worker.get_user_by_id(input_user_id)
        if user.password != input_password:
            self.logger.debug('"{wrong_password}" is not real password of user with id={user_id}.'.
                              format(wrong_password=input_password, user_id=input_user_id))
            raise WrongPasswordException('Password is wrong')

# ======================================================================================================================
# User section

    # Method for getting user's info by id
    def get_user_info_by_id(self, input_user_id):
        self.logger.debug('Trying to get user\'s info (without passwords ofc).')

        self.logger.debug('Searching user with id={user_id}.'.format(user_id=input_user_id))
        user = self.db_worker.get_user_by_id(input_user_id)

        return {'id': user.id, 'email': user.email, 'first_name': user.first_name, 'last_name': user.last_name}

    # Method for getting user's info by login
    def get_user_info_by_login(self, input_user_login):
        self.logger.debug('Trying to get user\'s info (without passwords ofc).')

        self.logger.debug('Searching user with login={user_login}.'.format(user_login=input_user_login))
        user = self.db_worker.get_user_by_login(input_user_login)

        return {'id': user.id, 'login': input_user_login, 'email': user.email, 'first_name': user.first_name,
                'last_name': user.last_name}

    # Method for getting list of all users info
    def get_users_info(self):
        self.logger.debug('Trying to get all user\'s info (without passwords ofc).')
        users = self.db_worker.get_all_users()

        self.logger.debug('Creating list of dictionaries.')
        users_info = []
        for user in users:
            users_info.append(dict(id=user.id, login=user.login, first_name=user.first_name, last_name=user.last_name,
                                   email=user.email))

        self.logger.debug('Returning info dictionary. {users_count} users were found.'.
                          format(users_count=len(users_info)))
        return users_info

    # Method for adding new user
    def user_add(self, input_login, input_first_name, input_last_name, input_email, input_password):
        self.logger.debug('Trying to add new user with login={user_login}.'.format(user_login=input_login))
        self.logger.debug('Checking info for user add operation.')
        Inspector.user_add_check(input_login, input_email, input_password, self.db_worker)

        user = User(login=input_login, first_name=input_first_name, last_name=input_last_name, email=input_email,
                    password=input_password)
        self.logger.debug('Adding user with login={user_login}.'.format(user_login=input_login))
        self.db_worker.db_session.add(user)

    # Edit user operation
    def user_edit(self, input_user_id, input_login, input_first_name, input_last_name, input_email, input_new_password,
                  input_old_password):
        self.logger.debug('Trying to edit user with id={user_id}.'.format(user_id=input_user_id))
        self.logger.debug('Checking info and rights for user edit operation.')
        Inspector.user_edit_check(self.executor_user_id, input_user_id, input_login, input_email, input_new_password,
                                  input_old_password, self.db_worker)

        user = self.db_worker.get_user_by_id(input_user_id)

        if input_login is not None:
            self.logger.debug('User with id={user_id} will have new login - {user_login}'.
                              format(user_id=input_user_id, user_login=input_login))
            user.login = input_login
        if input_first_name is not None:
            self.logger.debug('User with id={user_id} will have new first name - {user_first_name}'.
                              format(user_id=input_user_id, user_first_name=input_first_name))
            user.first_name = input_first_name
        if input_last_name is not None:
            self.logger.debug('User with id={user_id} will have new last name - {user_last_name}'.
                              format(user_id=input_user_id, user_last_name=input_last_name))
            user.last_name = input_last_name
        if input_email is not None:
            self.logger.debug('User with id={user_id} will have new email - {user_email}'.
                              format(user_id=input_user_id, user_email=input_email))
            user.email = input_email
        if input_new_password is not None:
            self.logger.debug('User with id={user_id} will have new password.')
            user.password = input_new_password

    # Remove user operation
    def user_remove(self, input_user_id, input_password):
        self.logger.debug('Trying to remove user with id={user_id}.'.format(user_id=input_user_id))
        self.logger.debug('Checking rights for user remove operation.')
        Inspector.user_remove_check(self.executor_user_id, input_user_id, input_password, self.db_worker)

        user = self.db_worker.get_user_by_id(input_user_id)
        self.logger.debug('Removing user with id={user_id}.'.format(user_id=input_user_id))
        self.db_worker.db_session.delete(user)

# ======================================================================================================================
# Task block

    # Method for getting list of all user's tasks info
    def get_task_info(self, input_task_id):
        self.logger.debug('Trying to get info of task with id={task_id}).'.format(task_id=input_task_id))

        self.logger.debug('Checking rights of user with id={user_id}.'.format(user_id=self.executor_user_id))
        task = self.db_worker.get_task(input_task_id)
        if self.executor_user_id != 0:
            user = self.db_worker.get_user_by_id(self.executor_user_id)
            if user not in task.users:
                raise NotEnoughRightsException('Not enough rights to get task\'s info.')

        script_id = None
        if len(task.script) != 0:
            script_id = task.script[0].id

        users = []
        for user in task.users:
            users.append({'id': user.id, 'login': user.login})

        return {'id': task.id, 'name': task.name, 'description': task.description,
                'start_datetime': task.start_datetime, 'end_datetime': task.end_datetime, 'users': users,
                'ownertask_id': task.ownertask_id, 'status': task.status[0].status, 'script_id': script_id}

    # Method for getting list of all user's tasks info
    def get_tasks_info(self):
        self.logger.debug('Trying get all executor\'s (id={user_id}) task\'s info.'.
                          format(user_id=self.executor_user_id))
        tasks = self.db_worker.get_all_tasks()

        self.logger.debug('Selecting necessary tasks.')
        if self.executor_user_id != 0:
            user = self.db_worker.get_user_by_id(self.executor_user_id)
            all_tasks = tasks.copy()
            tasks.clear()
            for task in all_tasks:
                if user in task.users:
                    tasks.append(task)

        tasks_info = []
        self.logger.debug('Creating list of dictionaries.')
        for task in tasks:
            script_id = None
            if len(task.script) != 0:
                script_id = task.script[0].id
            users = []
            for user in task.users:
                users.append({'id': user.id, 'login': user.login})
            tasks_info.append(dict(id=task.id, name=task.name, status=task.status[0].status,
                                   start_datetime=task.start_datetime, end_datetime=task.end_datetime,
                                   ownertask_id=task.ownertask_id, users=users, script_id=script_id,
                                   description=task.description))

        self.logger.debug('Returning info dictionary.{tasks_count} tasks were found.'.
                          format(tasks_count=len(tasks_info)))
        return tasks_info

    # Method for cheking task's
    def get_subtasks_info(self, input_task_id):
        self.logger.debug('Trying to check if task with id={task_id} has subtasks.'.format (task_id=input_task_id))

        subtasks = self.db_worker.get_subtasks(input_task_id)
        tasks_info = []
        for task in subtasks:
            script_id = None
            if len(task.script) != 0:
                script_id = task.script[0].id
            tasks_info.append(dict(id=task.id, name=task.name, status=task.status[0].status,
                                   start_datetime=task.start_datetime, end_datetime=task.end_datetime,
                                   ownertask_id=task.ownertask_id, script_id=script_id,
                                   description=task.description))
        return tasks_info

    # Method for adding new task
    def task_add(self, input_user_id, input_name, input_description, input_start_datetime, input_end_datetime):
        self.logger.debug('Trying to add task to user with id={user_id} by executor with id={executor_id}.'.
                          format(user_id=input_user_id, executor_id=self.executor_user_id))
        self.logger.debug('Checking info and rights for task add operation.')
        Inspector.task_add_check(self.executor_user_id, input_user_id, input_name, input_start_datetime,
                                 input_end_datetime)

        user = self.db_worker.get_user_by_id(input_user_id)

        task = Task(name=input_name, description=input_description, start_datetime=input_start_datetime,
                    end_datetime=input_end_datetime)
        task.users.append(user)

        self.logger.debug('Selecting status.')
        if input_start_datetime is not None and datetime.now() < input_start_datetime:
            status = self.db_worker.get_status('INACTIVE')
        else:
            status = self.db_worker.get_status('ACTIVE')
        task.status.append(status)

        self.logger.debug('Adding task to user with id={user_id}.'.format(user_id=input_user_id))
        self.db_worker.db_session.add(task)
        self.db_worker.db_session.flush()

        self.logger.debug('Changing rights.')
        user_task_relationship = self.db_worker.get_user_task_relationship(input_user_id, task.id)
        user_task_relationship.user_rights = 1

    # Method for editing task
    def task_edit(self, input_task_id, input_name, input_description, input_start_datetime, input_end_datetime):
        self.logger.debug('Trying to edit task with id={task_id} by executor with id={executor_id}.'.
                          format(task_id=input_task_id, executor_id=self.executor_user_id))
        self.logger.debug('Checking info and rights for task edit operation.')
        Inspector.task_edit_check(self.executor_user_id, input_task_id, input_name, input_start_datetime,
                                  input_end_datetime, self.db_worker)

        task = self.db_worker.get_task(input_task_id)

        if input_name is not None:
            self.logger.debug('Task with id={task_id} will have name - {task_name}.'.
                              format(task_id=input_task_id, task_name=input_name))
            task.name = input_name

        if input_description is not None:
            self.logger.debug('Task with id={task_id} will have new description.')
            task.description = input_description

        if input_start_datetime is not None:
            self.logger.debug('Task with id={task_id} will have start datetime - {task_start_datetime}.'.
                              format(task_id=input_task_id, task_start_datetime=input_start_datetime))
            task.start_datetime = input_start_datetime

        if input_end_datetime is not None:
            self.logger.debug('Task with id={task_id} will have end datetime - {task_end_datetime}.'.
                              format(task_id=input_task_id, task_end_datetime=input_end_datetime))
            task.end_datetime = input_end_datetime

        self.logger.debug('Checking new time logic.')
        Inspector.check_datetime_logic(task.start_datetime, task.end_datetime)

        self.logger.debug('Changing status.')
        if task.start_datetime is not None:
            if task.start_datetime < datetime.now():
                self.logger.debug('New status - ACTIVE.')
                self.status_change(task.id, 'ACTIVE')
            else:
                self.status_change(task.id, 'INACTIVE')
                self.logger.debug('New status - INACTIVE.')

    # Method for removing task
    def task_remove(self, input_task_id):
        self.logger.debug('Trying to remove task with id={task_id} by executor with id={executor_id}.'.
                          format(task_id=input_task_id, executor_id=self.executor_user_id))
        task = self.db_worker.get_task(input_task_id)

        try:
            self.logger.debug('Checking rights for task remove operation.')
            Inspector.task_remove_check(self.executor_user_id, input_task_id, self.db_worker)

        except NotEnoughRightsException:
            user = self.db_worker.get_user_by_id(self.executor_user_id)
            if user in task.users:
                self.logger.debug('Destruction relationship between user with id={user_id} and task with id={task_id}'.
                                  format(user_id=self.executor_user_id, task_id=input_task_id))
                task.users.remove(user)
            else:
                self.logger.debug('User with id={user_id} doesn\'t have enough rights to remove task with id={task_id}'.
                                  format(user_id=self.executor_user_id, task_id=input_task_id))
                raise

        subtasks = self.db_worker.get_subtasks(input_task_id)
        for subtask in subtasks:
            self.task_remove(subtask.id)

        self.logger.debug('Task with id={task_id} was removed.'.format(task_id=input_task_id))
        self.db_worker.db_session.delete(task)

    # Method for changing task's status
    def status_change(self, input_task_id, input_status_name):
        self.logger.debug('Trying to change status of task with id={task_id} by executor with id={executor_id}.'.
                          format(task_id=input_task_id, executor_id=self.executor_user_id))
        self.logger.debug('Checking info and rights for change task\'s status operation.')
        Inspector.status_change_check(self.executor_user_id, input_task_id, input_status_name, self.db_worker)

        task = self.db_worker.get_task(input_task_id)
        status = self.db_worker.get_status(input_status_name)

        task.status.clear()

        self.logger.debug('Changing status to {status_name} of task with id={task_id}'.
                          format(status_name=input_status_name, task_id=input_task_id))
        task.status.append(status)
        self.db_worker.db_session.flush()

        self.try_script(input_task_id)

    # Method for adding subuser
    def subuser_add(self, input_task_id, input_subuser_id, input_user_rights):
        self.logger.debug('Trying to add subuser with with id={user_id} to task with id={task_id} with rights-level='
                          '{rights} by executor with id={executor_id}.'.
                          format(user_id=input_subuser_id, rights=input_user_rights, task_id=input_task_id,
                                 executor_id=self.executor_user_id))
        self.logger.debug('Checking rights for subuser add operation.')
        Inspector.subuser_add_check(self.executor_user_id, input_task_id, input_subuser_id, self.db_worker)

        subuser = self.db_worker.get_user_by_id(input_subuser_id)

        task = self.db_worker.get_task(input_task_id)
        if subuser not in task.users:
            self.logger.debug('Adding subuser with id={user_id} to task with id={task_id}'.
                              format(user_id=input_subuser_id, task_id=input_task_id))
            task.users.append(subuser)
        self.db_worker.db_session.add(task)

        self.db_worker.db_session.flush()

        self.logger.debug('Changing rights level to {rights}.'.format(rights=input_user_rights))
        user_task_relationship = self.db_worker.get_user_task_relationship(input_subuser_id, input_task_id)
        user_task_relationship.user_rights = input_user_rights

        self.logger.debug('Checking subtasks.')
        subtasks = self.db_worker.get_subtasks(input_task_id)
        for subtask in subtasks:
            self.subuser_add(subtask.id, input_subuser_id, input_user_rights)

    # Method for removing subuser
    def subuser_remove(self, input_task_id, input_subuser_id):
        self.logger.debug('Trying to remove subuser with id={user_id} of task with id={task_id} by executor with id='
                          '{executor_id}.'.format(user_id=input_subuser_id, task_id=input_task_id,
                                                  executor_id=self.executor_user_id))
        self.logger.debug('Checking rights for subuser remove operation.')
        Inspector.subuser_remove_check(self.executor_user_id, input_task_id, input_subuser_id, self.db_worker)

        owneruser = self.db_worker.get_user_by_id(self.executor_user_id)

        task = self.db_worker.get_task(input_task_id)

        if input_subuser_id == 0:
            self.logger.debug('Removing all subusers of task with id={task_id} and it\'s subtasks besides user with id='
                              '{user_id}.'.format(task_id=input_task_id, user_id=self.executor_user_id))
            for subuser in task.users:
                if subuser.id is not owneruser.id:
                    task.users.remove(subuser)
                    if task.ownertask_id is not None:
                        self.subuser_remove(task.ownertask_id, subuser.id)

        else:
            self.logger.debug('Removing all subusers of task with id={task_id} and it\'s subtasks besides user with '
                              'id={user_id}'.format(task_id=input_task_id, user_id=self.executor_user_id))
            subuser = self.db_worker.get_user_by_id(input_subuser_id)
            if subuser in task.users:
                task.users.remove(subuser)

                if task.ownertask_id is not None:
                    self.subuser_remove(task.ownertask_id, input_subuser_id)

    # Method for adding subtask
    def subtask_add(self, input_ownertask_id, input_subtask_id):
        self.logger.debug('Trying to add subtask with id={subtask_id} to task with id='
                          '{ownertask_id} by executor with id={executor_id}.'.
                          format(subtask_id=input_subtask_id, ownertask_id=input_ownertask_id,
                                 executor_id=self.executor_user_id))
        self.logger.debug('Checking rights for subtask add operation.')
        Inspector.subtask_add_check(self.executor_user_id, input_ownertask_id, input_subtask_id, self.db_worker)

        subtask = self.db_worker.get_task(input_subtask_id)

        self.logger.debug('Adding subtask with id={subtask_id} to task with id={ownertask_id}.'.
                          format(subtask_id=input_subtask_id, ownertask_id=input_ownertask_id))
        subtask.ownertask_id = input_ownertask_id
        self.db_worker.db_session.add(subtask)

    # Method for removing subtask
    def subtask_remove(self, input_ownertask_id, input_subtask_id):
        self.logger.debug('Trying to remove subtask with id={subtask_id} to task with id='
                          '{ownertask_id} by executor with id={executor_id}.'.
                          format(subtask_id=input_subtask_id, ownertask_id=input_ownertask_id,
                                 executor_id=self.executor_user_id))
        self.logger.debug('Checking rights for subtask remove operation.')
        Inspector.subtask_remove_check(self.executor_user_id, input_ownertask_id, input_subtask_id, self.db_worker)

        if input_subtask_id == 0:
            self.logger.debug('Removing all subtasks of task with id={task_id}.'.format(task_id=input_ownertask_id))
            subtasks = self.db_worker.get_subtasks(input_ownertask_id)
            for subtask in subtasks:
                subtask.ownertask_id = None

        else:
            self.logger.debug('Removing subtask with id={subtask_id} of task with id={ownertask_id}.'.
                              format(subtask_id=input_subtask_id, ownertask_id=input_ownertask_id))
            subtask = self.db_worker.get_task(input_subtask_id)
            subtask.ownertask_id = None

# ======================================================================================================================
# Script section

    # Method for getting script's info
    def get_script_info(self, input_script_id):
        self.logger.debug('Trying to get info of script with id={script_id}.'.format(script_id=input_script_id))
        script = self.db_worker.get_script(input_script_id)

        return {'id': script.id, 'name': script.name, 'description': script.description,
                'start_static_delta': script.start_static_delta, 'end_static_delta': script.end_static_delta,
                'start_dynamic_delta': script.start_dynamic_delta, 'end_dynamic_delta': script.end_dynamic_delta,
                'backref': script.backref}


    # Method for changing task's script
    def script_change(self, input_task_id, input_task_name, input_task_description, input_start_static_days,
                      input_start_dynamic_days, input_end_static_days, input_end_dynamic_days, input_backref,
                      input_add_statuses, input_remove_statuses):
        self.logger.debug('Trying to change script of task with id={task_id} by executor with id={executor_id}.'.
                          format(task_id=input_task_id, executor_id=self.executor_user_id))
        self.logger.debug('Checking info and rights for change script operation.')
        Inspector.script_change_check(self.executor_user_id, input_task_id, input_task_name, input_add_statuses,
                                      input_remove_statuses, self.db_worker)

        task = self.db_worker.get_task(input_task_id)

        if input_start_static_days is None:
            input_start_static_days = 1
        if input_end_static_days is None:
            input_end_static_days = 1

        if input_task_name is None:
            input_task_name = task.name
        if input_task_description is None:
            input_task_description = task.description

        script = None
        if len(task.script) != 0:
            script = task.script[0]
        if script is None:
            self.logger.debug('Creating new script for task with id={task_id}.'.format(task_id=input_task_id))
            script = Script(name=input_task_name, description=input_task_description,
                            start_static_delta=input_start_static_days, start_dynamic_delta=input_start_dynamic_days,
                            end_static_delta=input_end_static_days, end_dynamic_delta=input_end_dynamic_days,
                            backref=input_backref)
            self.db_worker.db_session.add(script)
            task.script.append(script)
        else:
            self.logger.debug('Changing script of task with id={task_id}.'.format(task_id=input_task_id))
            script.name = input_task_name
            script.description = input_task_description
            if input_start_static_days is not None:
                script.start_static_delta = input_start_static_days
            if input_start_dynamic_days is not None:
                script.start_dynamic_delta = input_start_dynamic_days
            if input_end_static_days is not None:
                script.end_static_delta = input_end_static_days
            if input_end_dynamic_days is not None:
                script.end_dynamic_delta = input_end_dynamic_days
            if input_backref is not None:
                script.backref = input_backref

        if input_add_statuses is not None:
            for add_status in input_add_statuses:
                if add_status is not None and self.db_worker.get_status(add_status) not in script.statuses:
                    self.logger.debug('Adding status-trigger with name={status_name} to script of task with id='
                                      '{task_id}.'.format(status_name=add_status, task_id=input_task_id))
                    script.statuses.append(self.db_worker.get_status(add_status))
        if input_remove_statuses is not None:
            for rem_status in input_remove_statuses:
                self.logger.debug('Removing status-trigger with name={status_name} from script of task with id='
                                  '{task_id}.'.format(status_name=rem_status, task_id=input_task_id))
                if rem_status is not None and self.db_worker.get_status(rem_status) in script.statuses:
                    script.statuses.remove(self.db_worker.get_status(rem_status))

    # Method for removing task's script
    def script_remove(self, input_task_id):
        self.logger.debug('Trying to remove script of task with id={task_id} by executor with id={executor_id}.'.
                          format(task_id=input_task_id, executor_id=self.executor_user_id))
        self.logger.debug('Checking rights for remove script operation.')
        Inspector.script_remove_check(self.executor_user_id, input_task_id, self.db_worker)

        task = self.db_worker.get_task(input_task_id)

        script = task.script[0]
        if script is not None:
            self.logger.debug('Unhooking script from task with id={task_id}.'.format(task_id=input_task_id))
            task.script.clear()

        if script.tasks is None:
            self.logger.debug('Removing script with id={script_id}.'.format(script_id=script))
            self.db_worker.db_session.delete(script)

# ======================================================================================================================
# Perform section

    # Method for performing system tasks
    def perform(self):
        self.logger.info('System checking all tasks and changing statuses.')
        tasks = self.db_worker.get_all_tasks()

        for task in tasks:
            self.logger.debug('Performing method checking task with id={task_id}'.format(task_id=task.id))
            if (task.end_datetime is not None and task.end_datetime < datetime.now() and
                    task.status[0].status != 'COMPLETED' and task.status[0].status != 'OVERDUE'):
                self.logger.debug('Changing status in task with id={task_id} to OVERDUE.'.format(task_id=task.id))
                task.status.clear()
                task.status.append(self.db_worker.get_status('OVERDUE'))
                self.db_worker.db_session.flush()
                self.try_script(task.id)
            elif (task.start_datetime is not None and task.start_datetime < datetime.now() and
                  task.status[0].status != 'STOPPED' and task.status[0].status != 'ACTIVE'):
                self.logger.debug('Changing status in task with id={task_id} to ACTIVE.'.format(task_id=task.id))
                task.status.clear()
                task.status.append(self.db_worker.get_status('ACTIVE'))
                self.db_worker.db_session.flush()
                self.try_script(task.id)

    # Method for performing task's script
    def try_script(self, input_task_id):
        self.logger.debug('Trying to execute script of task with id={task_id}.'.format(task_id=input_task_id))
        task = self.db_worker.get_task(input_task_id)
        if len(task.script) == 0:
            return

        script = task.script[0]

        if task.status[0] in script.statuses:
            try:
                if script.start_dynamic_delta is not None and script.end_dynamic_delta is not None:
                    start_datetime = datetime.now() + timedelta(days=script.start_dynamic_delta)
                    end_datetime = datetime.now() + timedelta(days=script.end_dynamic_delta)
                elif script.start_dynamic_delta is not None:
                    start_datetime = datetime.now() + timedelta(days=script.start_dynamic_delta)
                    end_datetime = task.end_datetime + timedelta(days=script.end_static_delta)
                elif script.end_dynamic_delta is not None:
                    start_datetime = task.start_datetime + timedelta(days=script.start_static_delta)
                    end_datetime = datetime.now() + timedelta(days=script.end_dynamic_delta)
                else:
                    start_datetime = task.start_datetime + timedelta(days=script.start_static_delta)
                    end_datetime = task.end_datetime + timedelta(days=script.end_static_delta)
                Inspector.check_datetime_logic(start_datetime, end_datetime)
            except TimeLogicException:
                self.logger.debug('Time logic was broken. New task will have "None" start and end datetime.')
                start_datetime = None
                end_datetime = None

            self.logger.debug('Creating new task.')
            new_task = Task(name=script.name, description=script.description, start_datetime=start_datetime,
                            end_datetime=end_datetime)

            if new_task.start_datetime is None or new_task.start_datetime > datetime.now():
                self.logger.debug('New task will have ACTIVE status.')
                new_task.status.append(self.db_worker.get_status('ACTIVE'))
            elif new_task.start_datetime < datetime.now():
                self.logger.debug('New task will have INACTIVE status.')
                new_task.status.append(self.db_worker.get_status('INACTIVE'))

            if script.backref:
                self.logger.debug('Connecting new task to this script.')
                new_task.script.append(script)

            self.logger.debug('Adding new task.')
            self.db_worker.db_session.add(new_task)
            self.db_worker.db_session.flush()

            new_task.users = task.users
            for user in new_task.users:
                new_relationship = self.db_worker.get_user_task_relationship(user.id, new_task.id)
                old_relationship = self.db_worker.get_user_task_relationship(user.id, task.id)
                new_relationship.user_rights = old_relationship.user_rights
                self.db_worker.db_session.add(new_relationship)

            self.db_worker.db_session.delete(task)

# ======================================================================================================================
# Garbage collector section

    # Method for collecting garbage
    def collect_garbage(self):
        self.logger.info('System collecting garbage.')
        tasks = self.db_worker.get_all_tasks()
        for task in tasks:
            if len(task.users) == 0:
                self.logger.debug('Garbage collector removing task with id={task_id}.'.format(task_id=task.id))
                self.db_worker.db_session.delete(task)

        scripts = self.db_worker.get_all_scripts()
        for script in scripts:
            if len(script.tasks) == 0:
                self.logger.debug('Garbage collector removing script with id={script_id}.'.format(script_id=script.id))
                self.db_worker.db_session.delete(script)
