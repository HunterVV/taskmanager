from .controller import Controller


class CoreInterface:
    """
        To use CoreInterface methods u must set his db_address attribute first.
    """
    db_address = None

# ======================================================================================================================
# Basic operations section

    @staticmethod
    def get_user_id(input_login):
        """
        Function for getting user's id by login. It will raise UserMissingException if user won't be found.
        :param input_login: User's login
        :return: User's id
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_user_id(input_login)

    @staticmethod
    def get_user_login(input_user_id):
        """
        Function for getting user's login by id. It will raise UserMissingException if user won't be found.
        :param input_user_id: User's id
        :return: User's login
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_user_login(input_user_id)

    @staticmethod
    def get_my_task_id_by_name(input_executor_id, input_task_name):
        """
        Function for getting task's id by name. It will search only in executor's tasks. It will raise
        TaskMissingException if task won't be found or UncertaintyException if there more than 1 task with entered name.
        :param input_executor_id: Executor's (user's) id
        :param input_task_name: Task's name
        :return: Task's id
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.get_executor_task_id(input_task_name)

    @staticmethod
    def get_tasks_status_name(input_executor_id, input_task_id):
        """
        Function for getting task's status name by id. It will raise TaskMissingException if task won't be found or
        NotEnoughRightsException if executor doesn't have enough rights.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :return: Task's status name
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.get_tasks_status_name(input_task_id)

    @staticmethod
    def check_users_password(input_user_id, input_password):
        """
        Function for getting task's status name by id. It will raise UserMissingException if user won't be found or
        WrongPasswordException if entered password is wrong.
        :param input_user_id:
        :param input_password:
        :return:
        """
        controller = Controller(CoreInterface.db_address)
        return controller.check_users_password(input_user_id, input_password)

# ======================================================================================================================
# Common operations section

    @staticmethod
    def get_user_info_by_id(input_user_id):
        """
        Function for getting info user by id. It will return dictionaries, describing each users field.
        :return: Dictionary with info
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_user_info_by_id(input_user_id)

    @staticmethod
    def get_user_info_by_login(input_user_login):
        """
        Function for getting info user by login. It will return dictionaries, describing each users field.
        :return: Dictionary with info
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_user_info_by_login(input_user_login)

    @staticmethod
    def get_users_info():
        """
        Function for getting info of all users. It will return list of dictionaries, describing each user.
        :return: List of dictionaries with info
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_users_info()

    @staticmethod
    def user_add(input_login, input_first_name=None, input_last_name=None, input_email=None,
                 input_password=''):
        """
        Function for adding new user. User's login must be unique, otherwise it will raise UserAlreadyExistsException.
        If login or email can't be used it will raise InvalidNameException. If password is not empty, it's length must
        be between 4 and 24 symbols, otherwise it will raise InvalidNameException.
        :param input_login: Login
        :param input_first_name: First name
        :param input_last_name: Last name
        :param input_email: Email
        :param input_password: Password
        :return: None
        """
        controller = Controller(CoreInterface.db_address)
        return controller.user_add(input_login, input_first_name, input_last_name, input_email, input_password)

    @staticmethod
    def user_edit(input_executor_id, input_user_id, input_login=None, input_first_name=None, input_last_name=None,
                  input_email=None, input_new_password=None, input_old_password=None):
        """
        Function for editing user. New values will replace the old ones. User's login must be unique, otherwise it will
        raise UserAlreadyExistsException. If login or email can't be used it will raise InvalidNameException. If
        password is not empty, it's length must be between 4 and 24 symbols, otherwise it will raise
        InvalidNameException. New values will replace the old ones. Old password needed to enable other users to edit
        this. If executor doesn't have enough rights it will raise NotEnoughRightsException. If user won't be found it
        will raise UserMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_user_id: Target user's id
        :param input_login: New login
        :param input_first_name: New first name
        :param input_last_name: New last name
        :param input_email: New email
        :param input_new_password: New password
        :param input_old_password: Old password
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.user_edit(input_user_id, input_login, input_first_name, input_last_name, input_email,
                                    input_new_password, input_old_password)

    @staticmethod
    def user_remove(input_executor_id, input_user_id, input_password=None):
        """
        Function for removing user. Old password needed to enable other users to remove this. If executor doesn't have
        enough rights it will raise NotEnoughRightsException. If user won't be found it will raise UserMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_user_id: Target user's id
        :param input_password: Target user's password
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.user_remove(input_user_id, input_password)

    @staticmethod
    def get_task_info(input_executor_id, input_task_id):
        """
        Function for getting info of task. It will return dictionaries, describing each task's field.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task id
        :return: Dictionary with info
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.get_task_info(input_task_id)

    @staticmethod
    def get_tasks_info(input_executor_id):
        """
        Function for getting info of all executor's tasks. It will return list of dictionaries, describing each user's
        task.
        :param input_executor_id: Executor's (user's) id
        :return: List of dictionaries with info
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.get_tasks_info()

    @staticmethod
    def get_subtasks_info(input_task_id):
        """
        Function for getting info of all task's subtasks. It will return list of dictionaries, describing each task.
        :param input_task_id: Ownertask id
        :return: List of dictionaries with info
        """
        controller = Controller(CoreInterface.db_address)
        return controller.get_subtasks_info(input_task_id)

    @staticmethod
    def task_add(input_executor_id, input_user_id, input_name=None, input_description=None, input_start_datetime=None,
                 input_end_datetime=None):
        """
        Function for adding new task to user with id=input_user_id. If name can't be used it will raise
        InvalidNameException. Start datetime must be > now and end datetime must be > start datetime, if time logic is
        broken it will raise TimeLogicException. If executor doesn't have enough rights it will raise
        NotEnoughRightsException.
        :param input_executor_id: Executor's (user's) id
        :param input_user_id: Target user's id
        :param input_name: Task's name
        :param input_description: Task's description
        :param input_start_datetime: Task's start datetime
        :param input_end_datetime: Task's end datetime
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.task_add(input_user_id, input_name, input_description, input_start_datetime,
                                   input_end_datetime)

    @staticmethod
    def task_edit(input_executor_id, input_task_id, input_name=None, input_description=None, input_start_datetime=None,
                  input_end_datetime=None):
        """
        Function for editing task. If new name can't be used it will raise InvalidNameException. Start datetime must be
        > now and end datetime must be > start datetime, if time logic is broken it will raise TimeLogicException. If
        executor doesn't have enough rights it will raise NotEnoughRightsException. If task won't be found it will raise
        TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :param input_name: New task's name
        :param input_description: New task's description
        :param input_start_datetime: New task's start datetime
        :param input_end_datetime: New task's end datetime
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.task_edit(input_task_id, input_name, input_description, input_start_datetime,
                                    input_end_datetime)

    @staticmethod
    def task_remove(input_executor_id, input_task_id):
        """
        Function for removing task. If executor doesn't have enough rights it will raise NotEnoughRightsException. If
        task won't be found it will raise TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.task_remove(input_task_id)

    @staticmethod
    def status_change(input_executor_id, input_task_id, input_status_name):
        """
        Function for changing task's status if it's possible. If executor doesn't have enough rights it will raise
        NotEnoughRightsException. If task won't be found it will raise TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :param input_status_name: New status name
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.status_change(input_task_id, input_status_name)

    @staticmethod
    def subuser_add(input_executor_id, input_task_id, input_subuser_id, input_user_rights):
        """
        Function for adding subuser to task. If executor doesn't have enough rights it will raise
        NotEnoughRightsException. If subuser won't be found it will raise UserMissingException. If task won't be found
        it will raise TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :param input_subuser_id: Subuser's id
        :param input_user_rights: Subuser rights (0 or 1)
        :return:
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.subuser_add(input_task_id, input_subuser_id, input_user_rights)

    @staticmethod
    def subuser_remove(input_executor_id, input_task_id, input_subuser_id):
        """
        Function for removing task's subuser. If input_subuser_id=0 it will remove all subusers except executor. If
        executor doesn't have enough rights it will raise NotEnoughRightsException. If subuser won't be found it will
        raise UserMissingException. If task won't be found it will raise TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :param input_subuser_id: Subuser's id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.subuser_remove(input_task_id, input_subuser_id)

    @staticmethod
    def subtask_add(input_executor_id, input_ownertask_id, input_subtask_id):
        """
        Function for adding subtask to task. If executor doesn't have enough rights it will raise
        NotEnoughRightsException. If ownertask or subtask won't be found it will raise TaskMissingException. Ownertask
        and subtask shoudn't have scripts, otherwise it will raise ScriptLogicException.
        :param input_executor_id: Executor's (user's) id
        :param input_ownertask_id: Ownertask id
        :param input_subtask_id: Subtask id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.subtask_add(input_ownertask_id, input_subtask_id)

    @staticmethod
    def subtask_remove(input_executor_id, input_ownertask_id, input_subtask_id):
        """
        Function for removing task's subtask. If input_subtask_id=0 it will remove all subtasks. If executor doesn't
        have enough rights it will raise NotEnoughRightsException. If ownertask or subtask won't be found it will raise
        TaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_ownertask_id: Ownertask id
        :param input_subtask_id: Subtask id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.subtask_remove(input_ownertask_id, input_subtask_id)

    @staticmethod
    def get_script_info(input_executor_id, input_script_id):
        """
        Function for getting info of script. It will return dictionaries, describing each task's field.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Script id
        :return: Dictionary with info
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.get_script_info(input_script_id)

    @staticmethod
    def script_change(input_executor_id, input_task_id, input_task_name, input_task_description,
                      input_start_static_days, input_start_dynamic_days, input_end_static_days, input_end_dynamic_days,
                      input_backref, input_add_statuses, input_remove_statuses):
        """
        Function for creation or changing task's script. Task's script will create a new task if old task will change
        it's status to one of statuses in script's trigger list. New task will create with the help of information in
        script. To set start and end datetime it will use start_dynamic_days and end_dynamic_days fields or
        start_static_days and end_static_days if first ones are None. If backref is true, new task will set old task's
        script as his own. If task won't be found it will raiseTaskMissingException. Task shoudn't have subtasks or
        ownertask, otherwise it will raise ScriptLogicException. If executor doesn't have enough rights it will raise
        NotEnoughRightsException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :param input_task_name: Task's name in script
        :param input_task_description: Task's description in script
        :param input_start_static_days: Start static days
        :param input_start_dynamic_days: Start dynamic days
        :param input_end_static_days: End static days
        :param input_end_dynamic_days: End dynamic days
        :param input_backref: Reference from new task to script (bool)
        :param input_add_statuses: Trigger statuses(names) list to add to script
        :param input_remove_statuses: Trigger statuses(names) list to remove from script
        :return:
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.script_change(input_task_id, input_task_name, input_task_description, input_start_static_days,
                                        input_start_dynamic_days, input_end_static_days, input_end_dynamic_days,
                                        input_backref, input_add_statuses, input_remove_statuses)

    @staticmethod
    def script_remove(input_executor_id, input_task_id):
        """
        Function for removing task's script. If executor doesn'thave enough rights it will raise
        NotEnoughRightsException. If task won't be found it will raiseTaskMissingException.
        :param input_executor_id: Executor's (user's) id
        :param input_task_id: Task's id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        return controller.script_remove(input_task_id)

# ======================================================================================================================
# Background operations section

    @staticmethod
    def perform(input_executor_id):
        """
        Function for checking and changing all task's statuses if it's necessary. It will also try to execute scripts.
        :param input_executor_id: Executor's (user's) id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        controller.perform()

    @staticmethod
    def collect_garbage(input_executor_id):
        """
        Function for collecting garbage in database. It will remove abandoned tasks and scripts.
        :param input_executor_id: Executor's (user's) id
        :return: None
        """
        controller = Controller(CoreInterface.db_address, input_executor_id)
        controller.collect_garbage()
