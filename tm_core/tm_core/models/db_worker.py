from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import logging

from .entities import (User,
                       Task,
                       Status,
                       Script,
                       TaskUserRelationship)
from ..exeptions import (UserMissingException,
                         TaskMissingException,
                         StatusMissingException,
                         ScriptMissingException,
                         RelationshipMissingException)


class DatabaseWorker:
    """
    Class for working with database. It includes all basic operations for which sqlalchemy session is needed.
    """

    def __init__(self, input_address):
        self.logger = logging.getLogger('coreLogger' + __name__)
        self.logger.debug('Creating DatabaseWorker.')
        self.logger.debug('Creating sqlalchemy db_session.')
        connection_engine = create_engine(input_address, echo=False)
        connection_engine.connect()
        self.db_session = Session(connection_engine)

    def __del__(self):
        self.logger.debug('Closing sqlalchemy db_session.')
        self.db_session.commit()
        self.db_session.close()

# ======================================================================================================================
# Users operations section

    def get_user_by_id(self, input_user_id):
        user = self.db_session.query(User).filter(User.id == input_user_id).first()
        if user is None:
            self.logger.debug('User wasn\'t found.')
            raise UserMissingException('User with id={user_id} wasn\'t found.'.format(user_id=input_user_id))

        self.logger.debug('Returning user with id={user_id}.'.format(user_id=input_user_id))
        return user

    def get_user_by_login(self, input_login):
        user = self.db_session.query(User).filter(User.login == input_login).first()
        if user is None:
            self.logger.debug('User with id={user_login} wasn\'t found.'.format(user_login=input_login))
            raise UserMissingException('User wasn\'t found.')

        self.logger.debug('Returning user with login={user_login}.'.format(user_login=input_login))
        return user

    def get_all_users(self):
        users = self.db_session.query(User).all()

        self.logger.debug('Returning all user from database.')
        return users

# ======================================================================================================================
# Tasks operations section

    def get_task(self, input_task_id):
        task = self.db_session.query(Task).filter(Task.id == input_task_id).first()
        if task is None:
            self.logger.debug('Task with id={task_id} wasn\'t found.'.format(task_id=input_task_id))
            raise TaskMissingException('Task wasn\'t found.')

        self.logger.debug('Returning task with id={task_id}.'.format(task_id=input_task_id))
        return task

    def get_all_tasks(self):
        tasks = self.db_session.query(Task).all()

        self.logger.debug('Returning all tasks from database.')
        return tasks

    def get_subtasks(self, input_ownertask_id):
        subtasks = self.db_session.query(Task).filter(Task.ownertask_id == input_ownertask_id).all()

        self.logger.debug('Returning all subtasks of task with id={task_id}.'.format(task_id=input_ownertask_id))
        return subtasks

# ======================================================================================================================
# Relationships operations section

    def get_user_task_relationship(self, input_user_id, input_task_id):
        task_user_relationship = (self.db_session.query(TaskUserRelationship).
                                  filter(TaskUserRelationship.user_id == input_user_id).
                                  filter(TaskUserRelationship.task_id == input_task_id).first())
        if task_user_relationship is None:
            self.logger.debug('Relationship between user with id={user_id} and task with id={task_id} wasn\'t found.'.
                              format(user_id=input_user_id, task_id=input_task_id))
            raise RelationshipMissingException('Relationship between user and task wasn\'t found.')

        self.logger.debug('Returning relationship between user with id={user_id} and task with id={task_id}.'.
                          format(user_id=input_user_id, task_id=input_task_id))
        return task_user_relationship

# ======================================================================================================================
# Statuses operations section

    def get_status(self, input_status_name):
        status = self.db_session.query(Status).filter(Status.status == input_status_name).first()
        if status is None:
            self.logger.debug('Status with name={status_name} wasn\'t found.'.format(status_name=input_status_name))
            raise StatusMissingException('Status wasn\'t found.')

        self.logger.debug('Returning status with name={status_name}.'.format (status_name=input_status_name))
        return status

# ======================================================================================================================
# Scripts operations section

    def get_script(self, input_script_id):
        script = self.db_session.query(Script).filter(Script.id == input_script_id).first()
        if script is None:
            self.logger.debug('Script with id={script_id} wasn\'t found.'.format(script_id=input_script_id))
            raise ScriptMissingException('Status wasn\'t found.')

        self.logger.debug('Returning script with id={script_id}.'.format (script_id=input_script_id))
        return script

    def get_all_scripts(self):
        scripts = self.db_session.query(Script).all()

        self.logger.debug('Returning all scripts from database.')
        return scripts
