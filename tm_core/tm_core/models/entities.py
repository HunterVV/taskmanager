from sqlalchemy import *
from sqlalchemy.orm import (
    relationship, )
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


user_task_association_table = Table(
    'user_task_mapping',
    Base.metadata,
    Column('user_id', INTEGER, ForeignKey('users.id')),
    Column('task_id', INTEGER, ForeignKey('tasks.id')),
    Column('user_rights', INTEGER),
)


task_status_association_table = Table(
    'task_status_mapping',
    Base.metadata,
    Column('status_id', INTEGER, ForeignKey('statuses.id')),
    Column('task_id', INTEGER, ForeignKey('tasks.id')),
)


task_task_association_table = Table(
    'task_task_mapping',
    Base.metadata,
    Column('ownertask_id', INTEGER, ForeignKey('tasks.id')),
    Column('subtask_id', INTEGER, ForeignKey('tasks.id')),
)


task_script_association_table = Table(
    'task_script_mapping',
    Base.metadata,
    Column('task_id', INTEGER, ForeignKey('tasks.id')),
    Column('script_id', INTEGER, ForeignKey('scripts.id')),
)


script_status_association_table = Table(
    'script_status_mapping',
    Base.metadata,
    Column('script_id', INTEGER, ForeignKey('scripts.id')),
    Column('status_id', INTEGER, ForeignKey('statuses.id')),
)


class Status(Base):
    __tablename__ = 'statuses'

    id = Column(INTEGER, primary_key=True)
    status = Column(CHAR(20), nullable=False)
    description = Column(String(250))


class User(Base):
    __tablename__ = 'users'

    id = Column(INTEGER, primary_key=True)
    login = Column(String(20), nullable=False)
    first_name = Column(String(20))
    last_name = Column(String(20))
    email = Column(String(50))
    password = Column(String(24))

    tasks = relationship('Task', secondary=user_task_association_table, back_populates='users')


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(INTEGER, primary_key=True)
    name = Column(String(50), nullable=False)
    description = Column(String(250))
    ownertask_id = Column(INTEGER)
    start_datetime = Column(DateTime)
    end_datetime = Column(DateTime)

    users = relationship('User', secondary=user_task_association_table, back_populates='tasks')
    status = relationship('Status', secondary=task_status_association_table)
    script = relationship('Script', secondary=task_script_association_table, back_populates='tasks')


class Script(Base):
    __tablename__ = 'scripts'

    id = Column(INTEGER, primary_key=True)
    name = Column(String(50), nullable=False)
    description = Column(String(250))
    start_static_delta = Column(INTEGER)
    start_dynamic_delta = Column(INTEGER)
    end_static_delta = Column(INTEGER)
    end_dynamic_delta = Column(INTEGER)
    backref = Column(BOOLEAN)

    tasks = relationship('Task', secondary=task_script_association_table)
    statuses = relationship('Status', secondary=script_status_association_table)


class TaskUserRelationship(Base):
    __tablename__ = user_task_association_table

    id = Column(INTEGER, primary_key=True)
    user_id = Column(INTEGER)
    task_id = Column(INTEGER)
    user_rights = Column(INTEGER)
