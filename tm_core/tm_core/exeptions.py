"""
Library exeptions.
"""


class UserMissingException(BaseException):
    """User doesn't exist."""


class UserAlreadyExistsException(BaseException):
    """User with this login already exists."""


class TaskMissingException(BaseException):
    """Task doesn't exist."""


class InvalidNameException(BaseException):
    """The proposed name can't be used."""


class WrongPasswordException(BaseException):
    """The passwords is wrong."""


class RelationshipMissingException(BaseException):
    """Relationship doesn't exist."""


class NotEnoughRightsException(BaseException):
    """Not enough rights for editing or deleting"""


class StatusMissingException(BaseException):
    """Status with this name doesn't exist."""


class ScriptMissingException(BaseException):
    """Script doesn't exist."""


class UncertaintyException(BaseException):
    """Uncertainty was founded."""


class TimeLogicException(BaseException):
    """Time logic was broken."""


class ScriptLogicException(BaseException):
    """Script logic was broken."""
