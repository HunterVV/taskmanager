import argparse
import configparser
from datetime import (datetime,
                      timedelta)
import re
import logging

from . import config as pyconfig
from .exceptions import (WrongFormatException,
                         UserNotLoggedInException,
                         AuthorizationException)
from tm_core.exeptions import WrongPasswordException
from tm_core.control.core_interface import CoreInterface
from tm_core.control.inspector import Inspector

# parser for command line arguments
class Parser:

    def __init__(self):
        self.logger = logging.getLogger('consoleLogger.' + __name__)
        self.logger.debug('Parser creating.')

        config = configparser.RawConfigParser()
        config.read(pyconfig.CONFIG_PATH)
        self.user_id = config.get('authorization', 'user_id')
        self.password = config.get('authorization', 'password')
        self.db_address = pyconfig.DB_ADDRESS
        self.performing = pyconfig.AUTO_PERFORMING
        self.garbage_collecting = pyconfig.AUTO_GARBAGE_COLLECTING

    # Objects
    OBJECT = 'object'
    TASK = 'task'
    USER = 'user'
    LOGIN = 'login'
    LOGOUT = 'logout'

    # actions
    ACTION = 'action'
    SHOW = 'show'
    ADD = 'add'
    REMOVE = 'rm'
    EDIT = 'edit'
    COMPLETE = 'cmpl'
    STOP = 'stop'
    RESUME = 'res'
    ADD_SUBUSER = 'addsu'
    REMOVE_SUBUSER = 'rmsu'
    ADD_SUBTASK = 'addst'
    REMOVE_SUBTASK = 'rmst'
    CHANGE_SCRIPT = 'chsc'
    REMOVE_SCRIPT = 'rmsc'

    # create the top-level parser
    def parse(self):
        main_parser = argparse.ArgumentParser(description="Task tracker application.")
        subparcers_for_main_parser = \
            main_parser.add_subparsers(dest=Parser.OBJECT, help='object to deal with like user, task, login, logout')
        Parser.login_parser_configuration(subparcers_for_main_parser)
        Parser.logout_parser_configuration(subparcers_for_main_parser)
        Parser.user_parser_configuration(subparcers_for_main_parser)
        Parser.task_parser_configuration(subparcers_for_main_parser)
        try:
            CoreInterface.db_address = self.db_address
            self.logger.info('Parsing arguments.')
            args = main_parser.parse_args()
            self.logger.debug('Trying to identify object.')
            if args.object == Parser.LOGIN:
                self.execute_login(args)
            elif args.object == Parser.LOGOUT:
                self.execute_logout()
            elif args.object == Parser.USER:
                self.execute_user_action(args)
            elif args.object == Parser.TASK:
                self.execute_task_action(args)
            self.logger.info('Operation done successfully.')
            self.logger.debug('Trying to start garbage collection.')
            if self.garbage_collecting is True:
                self.logger.debug('Garbage collection will start.')
                CoreInterface.collect_garbage(None)
            return 0

        except Exception as e:
            print(e)
            # sys.stderr.write(str(e.args))
            self.logger.info('{e}. Application will be stopped.'.format(e=e))
            self.logger.info('{e}. Application will be stopped.'.format(e=e.args))
            self.logger.info('{e}. Application will be stopped.'.format(e=str(e)))
            self.logger.info('Application will be stopped.')
            return 1

    # LOGIN
    @staticmethod
    def login_parser_configuration(subparcers_for_main_parser):
        login_parser = subparcers_for_main_parser.add_parser(Parser.LOGIN)

        login_parser.add_argument('login', help='User\'s login.')
        login_parser.add_argument('-p', '--password', help='User\'s password, optional.')

    # LOGOUT
    @staticmethod
    def logout_parser_configuration(subparcers_for_main_parser):
        logout_parser = subparcers_for_main_parser.add_parser(Parser.LOGOUT)

    # USER
    @staticmethod
    def user_parser_configuration(subparcers_for_main_parser):
        user_parser = subparcers_for_main_parser.add_parser(Parser.USER)
        subparcers_for_task_parser = user_parser.add_subparsers(dest=Parser.ACTION, help='User operations.')

        list_user_parser = subparcers_for_task_parser.add_parser(Parser.SHOW)
        add_user_parser = subparcers_for_task_parser.add_parser(Parser.ADD)
        edit_user_parser = subparcers_for_task_parser.add_parser(Parser.EDIT)
        remove_user_parser = subparcers_for_task_parser.add_parser(Parser.REMOVE)

        # arguments for user show action

        # arguments for user creation action
        add_user_parser.add_argument('login', help='User\'s login.')
        add_user_parser.add_argument('-fn', '--first_name', help='User\'s first name.')
        add_user_parser.add_argument('-ln', '--last_name', help='User\'s last name.')
        add_user_parser.add_argument('-e', '--email', help='User\'s email.')
        add_user_parser.add_argument('-p', '--password', help='User\'s password.')

        # arguments for user editing action
        edit_user_parser.add_argument('-l', '--login', help='User\'s login.')
        edit_user_parser.add_argument('-fn', '--first_name', help='User\'s first name.')
        edit_user_parser.add_argument('-ln', '--last_name', help='User\'s last name.')
        edit_user_parser.add_argument('-e', '--email', help='User\'s email.')
        edit_user_parser.add_argument('-p', '--password', help='User\'s password.')

        # arguments for user deletion action
        remove_user_parser.add_argument('login', help='User\'s login.')
        remove_user_parser.add_argument('-p', '--password', help='User\'s password.')

    # TASK
    @staticmethod
    def task_parser_configuration(subparcers_for_main_parser):
        task_parser = subparcers_for_main_parser.add_parser(Parser.TASK)
        subparcers_for_task_parser = task_parser.add_subparsers(dest=Parser.ACTION, help='task operations')

        list_task_parser = subparcers_for_task_parser.add_parser(Parser.SHOW)
        add_task_parser = subparcers_for_task_parser.add_parser(Parser.ADD)
        edit_task_parser = subparcers_for_task_parser.add_parser(Parser.EDIT)
        remove_task_parser = subparcers_for_task_parser.add_parser(Parser.REMOVE)
        complete_task_parser = subparcers_for_task_parser.add_parser(Parser.COMPLETE)
        stop_task_parser = subparcers_for_task_parser.add_parser(Parser.STOP)
        resume_task_parser = subparcers_for_task_parser.add_parser(Parser.RESUME)
        addsubuser_task_parser = subparcers_for_task_parser.add_parser(Parser.ADD_SUBUSER)
        removesubuser_task_parser = subparcers_for_task_parser.add_parser(Parser.REMOVE_SUBUSER)
        addsubtask_task_parser = subparcers_for_task_parser.add_parser(Parser.ADD_SUBTASK)
        removesubtask_task_parser = subparcers_for_task_parser.add_parser(Parser.REMOVE_SUBTASK)
        changescript_task_parser = subparcers_for_task_parser.add_parser(Parser.CHANGE_SCRIPT)
        removescript_task_parser = subparcers_for_task_parser.add_parser(Parser.REMOVE_SCRIPT)

        # arguments for task show action

        # arguments for task creation action
        add_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')
        add_task_parser.add_argument('-d', '--description', help='Task\'s description.')
        add_task_parser.add_argument('-sd', '--start_date', help='Task\'s start date.')
        add_task_parser.add_argument('-st', '--start_time', help='Task\'s start time.')
        add_task_parser.add_argument('-ed', '--end_date', help='Task\'s end date.')
        add_task_parser.add_argument('-et', '--end_time', help='Task\'s end time.')

        # arguments for task editing action
        edit_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')
        edit_task_parser.add_argument('-n', '--name', help='Task\'s new name.')
        edit_task_parser.add_argument('-d', '--description', help='Task\'s desciption.')
        edit_task_parser.add_argument('-sd', '--start_date', help='Task\'s start date.')
        edit_task_parser.add_argument('-st', '--start_time', help='Task\'s start time.')
        edit_task_parser.add_argument('-ed', '--end_date', help='Task\'s end date.')
        edit_task_parser.add_argument('-et', '--end_time', help='Task\'s end time.')

        # arguments for task deletion action
        remove_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')

        # arguments for task execution action
        complete_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')

        # arguments for task stopping action
        stop_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')

        # arguments for task deletion action
        resume_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')

        # arguments for subuser adding action
        addsubuser_task_parser.add_argument('task', help='Task\'s id or name to identify it.')
        addsubuser_task_parser.add_argument('user', help='Subuser\'s id or name to identify it.')
        addsubuser_task_parser.add_argument('-r', '--rights', help='Subuser\'s rights.')

        # arguments for subuser removing action
        removesubuser_task_parser.add_argument('task', help='Task\'s id or name to identify it.')
        removesubuser_task_parser.add_argument('user', help='Subuser\'s id or name to identify it.')

        # arguments for subtask adding action
        addsubtask_task_parser.add_argument('ownertask', help='Ownertask\'s id or name to identify it.')
        addsubtask_task_parser.add_argument('subtask', help='Subtask\'s id or name to identify it.')

        # arguments for subtask removing action
        removesubtask_task_parser.add_argument('ownertask', help='Ownertask\'s id or name to identify it.')
        removesubtask_task_parser.add_argument('subtask', help='Subtask\'s id or name to identify it.')

        # arguments for aditing task script
        changescript_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')
        changescript_task_parser.add_argument('-n', '--name', help='New task\'s name.')
        changescript_task_parser.add_argument('-d', '--description', help='New task\'s description.')
        changescript_task_parser.add_argument('-ssd', '--start_static_days', '-ssd', help='Start static days.')
        changescript_task_parser.add_argument('-sdd', '--start_dynamic_days', '-sdd', help='Start dynamic days.')
        changescript_task_parser.add_argument('-esd', '--end_static_days', '-esd', help='End static days.')
        changescript_task_parser.add_argument('-edd', '--end_dynamic_days', '-edd', help='End dynamic days.')
        changescript_task_parser.add_argument('-b', '--backref', '-br', help='Back reference for new task.')
        changescript_task_parser.add_argument('-as', '--add_status', '-ads', action='append',
                                              help='Add new status trigger.')
        changescript_task_parser.add_argument('-rs', '--rem_status', '-rms', action='append',
                                              help='Remove status trigger.')

        # arguments for removing task script
        removescript_task_parser.add_argument('task_info', help='Task\'s id or name to identify it.')

# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================

    # Method for login operation
    def execute_login(self, args):
        self.logger.debug('Login operation started. ')
        user_id = CoreInterface.get_user_id(args.login)
        self.logger.debug('Trying to login user with user_id={user_id}.'.format(user_id=user_id))
        CoreInterface.check_users_password(user_id, args.password)
        config = configparser.RawConfigParser()
        config.read(pyconfig.CONFIG_PATH)
        config.set('authorization', 'user_id', CoreInterface.get_user_id(args.login))
        config.set('authorization', 'password', args.password)
        with open(pyconfig.CONFIG_PATH, "w") as config_file:
            config.write(config_file)
        self.logger.debug('User with user_id={user_id} logged in.'.format(user_id=user_id))

    # Method for logout operation
    def execute_logout(self):
        self.logger.debug('Logout operation started.')
        config = configparser.RawConfigParser()
        config.read(pyconfig.CONFIG_PATH)
        config.set('authorization', 'user_id', '')
        config.set('authorization', 'password', '')
        with open(pyconfig.CONFIG_PATH, "w") as config_file:
            config.write(config_file)
        self.logger.debug('Logout successful.')

# ======================================================================================================================

    # Method for choosing user operation
    def execute_user_action(self, args):
        self.logger.debug('Trying to identify user action.')
        if args.action == Parser.SHOW:
            self.execute_user_action_list()
        elif args.action == Parser.ADD:
            self.execute_user_action_add(args)
        elif args.action in Parser.REMOVE:
            self.execute_user_action_remove(args)
        elif args.action == Parser.EDIT:
            self.check_if_already_logged_in()
            self.execute_user_action_edit(args)

    # Method for choosing task operation
    def execute_task_action(self, args):
        self.logger.info('Checking logged in user.')
        self.check_if_already_logged_in()
        self.logger.debug('Trying to perform \'engine\'.')
        if self.performing is True:
            self.logger.debug('Engine perform will start.')
            CoreInterface.perform(None)
        self.logger.debug('Trying to identify task action.')
        if args.action == Parser.SHOW:
            self.execute_task_action_list()
        elif args.action == Parser.ADD:
            self.execute_task_action_add(args)
        elif args.action == Parser.EDIT:
            self.execute_task_action_edit(args)
        elif args.action == Parser.REMOVE:
            self.execute_task_action_remove(args)
        elif args.action == Parser.COMPLETE:
            self.execute_task_action_complete(args)
        elif args.action == Parser.STOP:
            self.execute_task_action_stop(args)
        elif args.action == Parser.RESUME:
            self.execute_task_action_resume(args)
        elif args.action == Parser.ADD_SUBUSER:
            self.execute_task_action_addsubuser(args)
        elif args.action == Parser.REMOVE_SUBUSER:
            self.execute_task_action_remove_subuser(args)
        elif args.action == Parser.ADD_SUBTASK:
            self.execute_task_action_add_subtask(args)
        elif args.action == Parser.REMOVE_SUBTASK:
            self.execute_task_action_remove_subtask(args)
        elif args.action == Parser.CHANGE_SCRIPT:
            self.execute_task_action_change_script(args)
        elif args.action == Parser.REMOVE_SCRIPT:
            self.execute_task_action_remove_script(args)

# ======================================================================================================================

    # Method for 'user show' operation
    def execute_user_action_list(self):
        self.logger.debug('User show operation started.')
        users_list = CoreInterface.get_users_info()
        self.logger.debug('User data received.')
        print('id   |   login   |   first_name   |   last_name   |   email')
        for user in users_list:
            print('{id} | {login} | {first_name} | {last_name} | {email}'.
                  format(id=user['id'], login=user['login'], first_name=user['first_name'], last_name=user['last_name'],
                         email=user['email']))

    # Method for 'user add' operation
    def execute_user_action_add(self, args):
        self.logger.debug('User add operation started.')
        if args.password is None:
            args.password = ''
        CoreInterface.user_add(args.login, args.first_name, args.last_name, args.email, args.password)
        self.logger.debug('User with login={login} was added.'.format(login=args.login))

    # Method for 'user edit' operation
    def execute_user_action_edit(self, args):
        self.logger.debug('User edit operation started.')
        CoreInterface.user_edit(self.user_id, self.user_id, args.login, args.first_name, args.last_name, args.email,
                                args.password, None)
        self.logger.debug('User with login={login} was edited.'.format(login=args.login))

    # Method for 'user remove' operation
    def execute_user_action_remove(self, args):
        self.logger.debug('User remove operation started.')
        if Inspector.is_int(args.login):
            raise WrongFormatException('Wrong login.')
        user_id = CoreInterface.get_user_id(args.login)
        CoreInterface.user_remove(self.user_id, user_id, args.password)
        self.execute_logout()
        self.logger.debug('User with login={login} was removed.'.format(login=args.login))

# ======================================================================================================================

    # Method for 'task show' operation
    def execute_task_action_list(self):
        self.logger.debug('Task show operation started.')
        tasks_list = CoreInterface.get_tasks_info(self.user_id)
        self.logger.debug('Task data was received.')
        print('id   |    name    |   status    |     start datetime     |     end datetime     |  ownertask id  |'
              '  script id  |   description')
        for task in tasks_list:
            print('{id} | {name} | {status} | {start_datetime} | {end_datetime} | {ownertask_id} | {script_id} | '
                  '{description}'.format(id=task['id'], name=task['name'], status=task['status'],
                                         start_datetime=task['start_datetime'], end_datetime=task['end_datetime'],
                                         ownertask_id=task['ownertask_id'], script_id=task['script_id'],
                                         description=task['description']))

    # Method for 'task add' operation
    def execute_task_action_add(self, args):
        self.logger.debug('Task add operation started.')
        Parser.check_date_format(args.start_date)
        Parser.check_time_format(args.start_time)
        Parser.check_date_format(args.end_date)
        Parser.check_time_format(args.end_time)
        start_datetime = Parser.get_start_datetime(args.start_date, args.start_time)
        end_datetime = Parser.get_end_datetime(args.end_date, args.end_time)
        CoreInterface.task_add(self.user_id, self.user_id, args.task_info, args.description, start_datetime,
                               end_datetime)
        self.logger.debug('Task with name={name} was added.'.format(name=args.task_info))

    # Method for 'task edit' operation
    def execute_task_action_edit(self, args):
        self.logger.debug('Task edit operation started.')
        Parser.check_date_format(args.start_date)
        Parser.check_time_format(args.start_time)
        Parser.check_date_format(args.end_date)
        Parser.check_time_format(args.end_time)
        start_datetime = Parser.get_start_datetime(args.start_date, args.start_time)
        end_datetime = Parser.get_end_datetime(args.end_date, args.end_time)
        if not (Inspector.is_int(args.task_info)):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.task_edit(self.user_id, args.task_info, args.name, args.description, start_datetime, end_datetime)
        self.logger.debug('Task with task_id={task_id} was edited.'.format(task_id=args.task_info))

    # Method for 'task remove' operation
    def execute_task_action_remove(self, args):
        self.logger.debug('Task remove operation started.')
        if not (Inspector.is_int(args.task_info)):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.task_remove(self.user_id, args.task_info)
        self.logger.debug('Task with task_id={task_id} was removed.'.format(task_id=args.task_info))

    # Method for 'task execute' operation
    def execute_task_action_complete(self, args):
        self.logger.debug('Task complete operation started.')
        if not (Inspector.is_int(args.task_info)):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.status_change(self.user_id, args.task_info, 'COMPLETED')
        self.logger.debug('Task with task_id={task_id} was completed.'.format(task_id=args.task_info))

    # Method for 'task execute' operation
    def execute_task_action_stop(self, args):
        self.logger.debug('Task stop operation started.')
        if not (Inspector.is_int(args.task_info)):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.status_change(self.user_id, args.task_info, 'STOPPED')
        self.logger.debug('Task with task_id={task_id} was stopped.'.format(task_id=args.task_info))

    # Method for 'task execute' operation
    def execute_task_action_resume(self, args):
        self.logger.debug('Task resume operation started.')
        if not (Inspector.is_int(args.task_info)):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.status_change(self.user_id, args.task_info, 'ACTIVE')
        self.logger.debug('Task with task_id={task_id} was resumed.'.format(task_id=args.task_info))

    # Method for 'task add subuser' operation
    def execute_task_action_addsubuser(self, args):
        self.logger.debug('Task addsubuser operation started.')
        if args.rights is None:
            args.rights = 0
        if not (Inspector.is_int(args.task)):
            args.task = CoreInterface.get_my_task_id_by_name(self.user_id, args.task)
        if not (Inspector.is_int(args.user)):
            args.user = CoreInterface.get_user_id(args.user)
        CoreInterface.subuser_add(self.user_id, args.task, args.user, args.rights)
        self.logger.debug('Subuser with user_id={user_id} was added to task with task_id={task_id}.'.
                          format(user_id=args.user, task_id=args.task))

    # Method for 'task remove subuser' operation
    def execute_task_action_remove_subuser(self, args):
        self.logger.debug('Task remove subuser operation started.')
        if str(args.user).upper() == 'ME':
            args.user = self.user_id
        if str(args.user).upper() == 'ALL':
            args.user = 0
        if not (Inspector.is_int(args.task)):
            args.task = CoreInterface.get_my_task_id_by_name(self.user_id, args.task)
        if not (Inspector.is_int(args.user)):
            args.user = CoreInterface.get_user_id(args.user)
        CoreInterface.subuser_remove(self.user_id, args.task, args.user)
        self.logger.debug('Subuser with user_id={user_id} was removed from task with task_id={task_id}.'.
                          format(user_id=args.user, task_id=args.task))

    # Method for 'task add subtask' operation
    def execute_task_action_add_subtask(self, args):
        self.logger.debug('Task add subtask operation started.')
        if not Inspector.is_int(args.ownertask):
            args.ownertask = CoreInterface.get_my_task_id_by_name(self.user_id, args.ownertask)
        if not Inspector.is_int(args.subtask):
            args.subtask = CoreInterface.get_my_task_id_by_name(self.user_id, args.subtask)
        CoreInterface.subtask_add(self.user_id, args.ownertask, args.subtask)
        self.logger.debug('Subtask with task_id={subtask_id} was added to ownertask with task_id={ownertask_id}.'.
                          format(subtask_id=args.subtask, ownertask_id=args.ownertask))

    # Method for 'task remove subtask' operation
    def execute_task_action_remove_subtask(self, args):
        self.logger.debug('Task removesubtask operation started.')
        if str(args.subtask).upper() == 'ALL':
            args.subtask = 0
        if not (Inspector.is_int(args.ownertask)):
            args.ownertask = CoreInterface.get_my_task_id_by_name(self.user_id, args.ownertask)
        if not (Inspector.is_int(args.subtask)):
            args.subtask = CoreInterface.get_my_task_id_by_name(self.user_id, args.subtask)
        CoreInterface.subtask_remove(self.user_id, args.ownertask, args.subtask)
        self.logger.debug('Subtask with task_id={subtask_id} was removed from ownertask with task_id={ownertask_id}.'.
                          format(subtask_id=args.subtask, ownertask_id=args.ownertask))

    # Method for 'task change script' operation
    def execute_task_action_change_script(self, args):
        self.logger.debug('Task changescript operation started.')
        if args.start_static_days is not None:
            if not Inspector.is_int(args.start_static_days):
                raise WrongFormatException('Days argument must be int.')
        if args.start_dynamic_days is not None:
            if not Inspector.is_int(args.start_dynamic_days):
                raise WrongFormatException('Days argument must be int.')
        if args.end_static_days is not None:
            if not Inspector.is_int(args.end_static_days):
                raise WrongFormatException('Days argument must be int.')
        if args.end_dynamic_days is not None:
            if not Inspector.is_int(args.end_dynamic_days):
                raise WrongFormatException('Days argument must be int.')
        if args.backref is not None:
            args.backref = bool(args.backref)
        else:
            args.backref = False
        if not Inspector.is_int(args.task_info):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.script_change(self.user_id, args.task_info, args.name, args.description, args.start_static_days,
                                    args.start_dynamic_days, args.end_static_days, args.end_dynamic_days, args.backref,
                                    args.add_status, args.rem_status)
        self.logger.debug('Task\'s script was changed (task_id={task_id}).'.format(task_id=args.task_info))

    # Method for 'task remove script' operation
    def execute_task_action_remove_script(self, args):
        self.logger.debug('Task remove script operation started.')
        if not Inspector.is_int(args.task_info):
            args.task_info = CoreInterface.get_my_task_id_by_name(self.user_id, args.task_info)
        CoreInterface.script_remove(self.user_id, args.task_info)
        self.logger.debug('Task\'s script was removed (task_id={task_id}).'.format(task_id=args.task_info))

# ======================================================================================================================

    # Method for checking logged in user
    def check_if_already_logged_in(self):
        self.logger.debug('Checking logged in user.')
        if self.user_id == '':
            raise UserNotLoggedInException('Log in first.')
        else:
            try:
                CoreInterface.check_users_password(self.user_id, self.password)
            except WrongPasswordException:
                raise AuthorizationException('User wasn\'t authorized.')

    # Method for checking input data format
    @staticmethod
    def check_date_format(input_date):
        if input_date is not None:
            if not (re.match('\d{2}.\d{2}.\d{4}', input_date) or re.match('\d.\d{2}.\d{4}', input_date) or
                    re.match('\d{2}.\d.\d{4}', input_date) or re.match('\d.\d.\d{4}', input_date)):
                raise WrongFormatException('Wrong data format.')

    # Method for checking input time format
    @staticmethod
    def check_time_format(input_time):
        if input_time is not None:
            if not (re.match('\d{2}:\d{2}', input_time) or re.match('\d{2}.\d{2}', input_time)):
                raise WrongFormatException('Wrong time format.')

    # Method for getting start datetime
    @staticmethod
    def get_start_datetime(input_date, input_time):
        if input_date is None and input_time is None:
            return datetime.now()
        elif input_date is None:
            return datetime.combine(datetime.today().date(), datetime.strptime(input_time, '%H:%M').time())
        elif input_time is None:
            return datetime.combine(datetime.strptime(input_date, '%d.%m.%Y').date(), datetime.now().time())
        else:
            return datetime.strptime(input_date + ' ' + input_time, '%d.%m.%Y %H:%M')

    # Method for getting end datetime
    @staticmethod
    def get_end_datetime(input_date, input_time):
        if input_date is None and input_time is None:
            return None
        elif input_date is None:
            return datetime.combine(datetime.today().date() + timedelta(days=1), datetime.strptime(input_time, '%H:%M').
                                    time())
        elif input_time is None:
            return datetime.combine(datetime.strptime(input_date, '%d.%m.%Y').date(), datetime(0, 0, 0, 0, 0).time())
        else:
            return datetime.strptime(input_date + ' ' + input_time, '%d.%m.%Y %H:%M')
