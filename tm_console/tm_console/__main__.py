from .tm_interpreter import Parser
from . import config
import logging
import logging.config
import sys
import configparser
import os


def run():
    set_loggers()
    check_configfile()
    logger = logging.getLogger('consoleLogger.' + __name__)
    logger.info('Calling console application.')
    parser = Parser()
    exit_code = parser.parse()
    sys.exit(exit_code)


def set_loggers():
    logging_dict = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters':
    {
        'simpleFormatter':
        {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },

    'handlers':
    {
        'consoleHandler':
        {
            'class': 'logging.StreamHandler',
            'formatter': 'simpleFormatter',
            'stream': 'ext://sys.stdout'
        },
        'fileHandler':
        {
            'class': 'logging.FileHandler',
            'formatter': 'simpleFormatter',
            'filename': config.LOGFILE_PATH,
        }
    },

    'loggers':
    {
        'consoleLogger':
        {
            'handlers': ['fileHandler'],
            'level': config.CONSOLE_LOGGER_LEVEL
        },
        'coreLogger':
        {
            'handlers': ['fileHandler'],
            'level': config.CORE_LOGGER_LEVEL
        }
    }}
    logging.config.dictConfig(logging_dict)


def check_configfile():
    if not os.path.exists(config.CONFIG_PATH):
        config_parser = configparser.ConfigParser()
        configfile = open(config.CONFIG_PATH, 'w')
        config_parser.add_section('authorization')
        config_parser.set('authorization', 'user_id', '')
        config_parser.set('authorization', 'password', '')
        config_parser.write(configfile)
        configfile.close()


if __name__ == "__main__":
    run()
