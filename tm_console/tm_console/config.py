import os

ROOT_PATH = os.path.expanduser('~')
CONFIG_NAME = 'config.cfg'

CONFIG_PATH = os.path.join(ROOT_PATH, CONFIG_NAME)

DB_ADDRESS = 'mysql+pymysql://root@127.0.0.1:3306/tsktrack'

AUTO_PERFORMING = True
AUTO_GARBAGE_COLLECTING = True

CONSOLE_LOGGER_LEVEL = 'DEBUG'
CORE_LOGGER_LEVEL = 'DEBUG'

LOGFILE_NAME = 'logfile.log'
LOGFILE_PATH = os.path.join(ROOT_PATH, LOGFILE_NAME)

