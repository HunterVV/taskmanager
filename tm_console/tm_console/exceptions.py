"""
Exception definitions.
"""


class AuthorizationException(BaseException):
    """Wrong login or password."""


class UserNotLoggedInException(BaseException):
    """User was not authorized using login action."""


class WrongFormatException(BaseException):
    """Wrong input data format."""
