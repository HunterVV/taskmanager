from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='tm_console',
    version='1.0.0',
    author = "Vitaly Vasilyeu",
    description='Console application to manage tasks.',
    packages=find_packages(),
)
