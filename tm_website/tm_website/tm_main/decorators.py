from django.http import HttpResponseRedirect
from django.contrib import auth


def tm_main_decorator(input_function):

    def function_wrapper(request, *args, **kwargs):
        #logger = logging.getLogger('consoleLogger.' + __name__)
        if auth.get_user(request).get_username() == '':
            return HttpResponseRedirect('/')
        try:
            return input_function(request, *args, **kwargs)
        except BaseException as e:
            print(e)
            return HttpResponseRedirect('/')

    return function_wrapper
