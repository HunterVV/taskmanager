from django.conf.urls import url, include
from django.contrib.auth import views as auth_views

from . import views


app_name = 'tm_main'

urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^login/$', auth_views.login, name='login'),
        url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
        url(r'^signup/$', views.signup, name='signup'),
        url(r'^tasks/$', views.show_tasks, name='show_tasks'),
        url(r'^tasks/(?P<task_id>[0-9]+)/$', views.edit_task, name='edit_task'),
        url(r'^tasks/(?P<task_id>[0-9]+)/remove$', views.task_remove, name='remove_task'),
        url(r'^tasks/(?P<task_id>[0-9]+)/scriptadd', views.script_add, name='add_script'),
        url(r'^tasks/(?P<task_id>[0-9]+)/scriptremove', views.script_remove, name='remove_script'),
        url(r'^tasks/(?P<ownertask_id>[0-9]+)/subtaskadd$', views.subtask_add, name='remove_task'),
        url(r'^tasks/(?P<task_id>[0-9]+)/useradd/$', views.add_tasks_user, name='add_user'),
        url(r'^tasks/(?P<task_id>[0-9]+)/userremove/(?P<subuser_id>[0-9]+)$',
            views.remove_tasks_user, name='remove_user'),
        url(r'^tasks/(?P<task_id>[0-9]+)/status/(?P<status_name>\w+)$', views.status_change, name='change_status'),
        url(r'^users/$', views.show_users, name='show_users'),
        url(r'^users/edit$', views.edit_myself, name='edit_myself'),
        url(r'^help/$', views.help, name='help'),
        url(r'^links/$', views.links, name='links'),
]