from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from datetime import datetime
from tempus_dominus.widgets import DateTimePicker


class UserAuthorizationForm(forms.Form):

    username = forms.CharField(max_length=24, required=True)
    password = forms.CharField(max_length=24, required=True)


class UserEditForm(forms.Form):

    id = forms.IntegerField(required=True)
    first_name = forms.CharField(max_length=128, required=False)
    last_name = forms.CharField(max_length=128, required=False)
    email = forms.EmailField(max_length=128, required=False)


class TaskEditForm(forms.Form):

    id = forms.IntegerField(required=True)
    name = forms.CharField(max_length=24, required=False)
    description = forms.CharField(max_length=250, required=False)
    #start_datetime = forms.DateTimeField(required=False, input_formats=['%d/%m/%Y %H:%M'], widget=DateTimePicker(options={'format':  'YYYY-MM-DD'}))
    start_datetime = forms.DateTimeField(required=False, input_formats=['%d.%m.%Y %H:%M', '%d/%m/%Y %H:%M',
                                                                        '%d.%m.%Y %H-%M', '%d/%m/%Y %H-%M'],
                                         widget=DateTimePicker())
    end_datetime = forms.DateTimeField(required=False, input_formats=['%d.%m.%Y %H:%M', '%d/%m/%Y %H:%M',
                                                                      '%d.%m.%Y %H-%M', '%d/%m/%Y %H-%M'])

    ownertask_id = forms.IntegerField(required=False)

    script_id = forms.IntegerField(required=False)
    days_delta = forms.IntegerField(required=False)


class SubuserAddForm(forms.Form):

    user_info = forms.CharField (max_length=24, required=True)


class TaskRemoveForm(forms.Form):

    id = forms.IntegerField(required=True)


class StatusChangeForm(forms.Form):

    task_id = forms.IntegerField(required=True)
    status = forms.CharField(required=True)


class TaskSortForm(forms.Form):

    show_active = forms.BooleanField(required=False)
    show_inactive = forms.BooleanField(required=False)
    show_stopped = forms.BooleanField(required=False)
    show_completed = forms.BooleanField(required=False)
    show_overdue = forms.BooleanField(required=False)

    sort_mode = forms.CharField(required=False)
