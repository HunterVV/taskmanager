from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader, RequestContext
from django.contrib import auth
from django.shortcuts import render
from django.contrib.auth import login, authenticate

from .forms import (UserAuthorizationForm,
                    UserEditForm,
                    TaskEditForm,
                    SubuserAddForm,
                    TaskSortForm)
from .decorators import tm_main_decorator

from django.contrib.auth.forms import UserCreationForm
from datetime import datetime
import pytz

from tm_core.control.core_interface import CoreInterface
from tm_core.control.inspector import Inspector


def index(request):
    return render(request, 'tm_main/index.html')


def authorization(request):
    if request.method == 'POST':
        form = UserAuthorizationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            login(request, user)
            return render(request, 'tm_main/index.html')

    else:
        form = UserAuthorizationForm()
    return render(request, 'registration/login.html', {'form': form})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')

            CoreInterface.user_add(input_login=username, input_password=raw_password)

            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request, 'tm_main/index.html')

    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@tm_main_decorator
def show_tasks(request):
    CoreInterface.perform(0)
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    tasks_info = CoreInterface.get_tasks_info(user_id)

    if request.method == 'GET':

        show_active = True
        show_inactive = True
        show_stopped = True
        show_completed = True
        show_overdue = True
        sort_mode = 'No sort'

        if 'show_button' in request.GET:
            show_buttons_list = request.GET.getlist('show_button', None)
            if 'show_active' in show_buttons_list:
                show_active = True
            else:
                show_active = False
            if 'show_inactive' in show_buttons_list:
                show_inactive = True
            else:
                show_inactive = False
            if 'show_stopped' in show_buttons_list:
                show_stopped = True
            else:
                show_stopped = False
            if 'show_completed' in show_buttons_list:
                show_completed = True
            else:
                show_completed = False
            if 'show_overdue' in show_buttons_list:
                show_overdue = True
            else:
                show_overdue = False

        if 'sort_mode' in request.GET:
            sort_mode = request.GET['sort_mode']

        if sort_mode == 'Sort by name':
            tasks_info.sort(key=lambda i: i['name'])
        elif sort_mode == 'Sort by id':
            tasks_info.sort(key=lambda i: i['id'])
        elif sort_mode == 'Sort by status':
                tasks_info.sort(key=lambda i: i['status'])

        tasks_to_remove = []
        if not show_active:
            for task in tasks_info:
                if task['status'] == 'ACTIVE':
                    tasks_to_remove.append(task)
        if not show_inactive:
            for task in tasks_info:
                if task['status'] == 'INACTIVE':
                    tasks_to_remove.append(task)
        if not show_stopped:
            for task in tasks_info:
                if task['status'] == 'STOPPED':
                    tasks_to_remove.append(task)
        if not show_completed:
            for task in tasks_info:
                if task['status'] == 'COMPLETED':
                    tasks_to_remove.append(task)
        if not show_overdue:
            for task in tasks_info:
                if task['status'] == 'OVERDUE':
                    tasks_to_remove.append(task)
        print(tasks_to_remove)
        for task in tasks_to_remove:
            if task in tasks_info:
                tasks_info.remove(task)

        context = {'show_active': show_active, 'show_inactive': show_inactive, 'show_stopped': show_stopped,
                   'show_completed': show_completed, 'show_overdue': show_overdue, 'sort_mode': sort_mode,
                   'tasks_info': tasks_info}
        return render(request, 'tm_main/task/show_tasks.html', context)

    return HttpResponseRedirect('/')


@tm_main_decorator
def edit_task(request, task_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    if request.method == 'POST':

        form = TaskEditForm(request.POST)
        user_form = SubuserAddForm(request.POST)
        if form.is_valid():

            task_id = form.cleaned_data.get('id')
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')
            start_datetime = form.cleaned_data.get('start_datetime')
            end_datetime = form.cleaned_data.get('end_datetime')
            script_id = form.cleaned_data.get('script_id')
            days_delta = form.cleaned_data.get('days_delta')

            if start_datetime is not None:
                start_datetime = start_datetime.replace(tzinfo=None)
            if end_datetime is not None:
                end_datetime = end_datetime.replace(tzinfo=None)

            if task_id == 0:
                CoreInterface.task_add(user_id, user_id, name, description, start_datetime, end_datetime)
            else:
                CoreInterface.task_edit(user_id, task_id, name, description, start_datetime, end_datetime)

            if script_id is not None:
                CoreInterface.script_change(user_id, task_id, name, description, days_delta, None, days_delta, None,
                                            True, None, None)

            return HttpResponseRedirect('/tasks/')

        task_info = CoreInterface.get_task_info(user_id, form.cleaned_data.get('id'))
        subtasks_info = CoreInterface.get_subtasks_info(task_info['id'])
        ownertask_info = None
        if task_info['ownertask_id'] is not None:
            ownertask_info = CoreInterface.get_task_info(user_id, task_info['ownertask_id'])

        context = {'form': form, 'users': task_info['users'], 'user_form': user_form, 'subtasks_info': subtasks_info,
                   'ownertask_info': ownertask_info}

    else:
        if task_id != '0' and task_id != 0:
            task_info = CoreInterface.get_task_info(user_id, task_id)

            # Collecting subtasks and ownertask info
            subtasks_info = CoreInterface.get_subtasks_info(task_info['id'])
            ownertask_info = None
            if task_info['ownertask_id'] is not None:
                ownertask_info = CoreInterface.get_task_info(user_id, task_info['ownertask_id'])

            # Collecting script info
            days_delta = 1
            if task_info['script_id'] is not None:
                script_info = CoreInterface.get_script_info(user_id, task_info['script_id'])
                if script_info['start_static_delta'] is not None:
                    days_delta = script_info['start_static_delta']
                if script_info['end_static_delta'] is not None:
                    days_delta = script_info['end_static_delta']

            # Creating forms
            form = TaskEditForm(initial={'id': task_info['id'], 'name': task_info['name'],
                                         'description': task_info['description'],
                                         'start_datetime': task_info['start_datetime'],
                                         'end_datetime': task_info['end_datetime'],
                                         'ownertask_id': task_info['ownertask_id'],
                                         'script_id': task_info['script_id'],
                                         'days_delta': days_delta})
            user_form = SubuserAddForm()

            context = {'form': form, 'users': task_info['users'], 'user_form': user_form,
                       'subtasks_info': subtasks_info, 'ownertask_info': ownertask_info}

        else:
            form = TaskEditForm(initial={'id': 0, 'name': 'NewName', 'description': '',
                                         'ownertask_id': None, 'script_id': None})
            context = {'form': form}

    return render(request, 'tm_main/task/edit_task.html', context)


@tm_main_decorator
def task_remove(request, task_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    CoreInterface.task_remove(user_id, task_id)

    return HttpResponseRedirect('/tasks/')


@tm_main_decorator
def script_add(request, task_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    task_info = CoreInterface.get_task_info(user_id, task_id)
    CoreInterface.script_change(user_id, task_id, task_info['name'], task_info['description'], 1, None, 1, None, True,
                                ['COMPLETED', 'OVERDUE'], None)

    return HttpResponseRedirect('/tasks/{task_id}'.format(task_id=task_id))


@tm_main_decorator
def script_remove(request, task_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    CoreInterface.script_remove(user_id, task_id)

    return HttpResponseRedirect('/tasks/{task_id}'.format(task_id=task_id))


@tm_main_decorator
def add_tasks_user(request, task_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    if request.method == 'POST':
        form = SubuserAddForm(request.POST)
        if form.is_valid():
            user_info = form.cleaned_data.get('user_info')
            subuser_id = user_info
            if not Inspector.is_int(user_info):
                subuser_id = CoreInterface.get_user_id(subuser_id)

            CoreInterface.subuser_add(user_id, task_id, subuser_id, 1)

            CoreInterface.subuser_add(user_id, task_id, 2, 1)
            return HttpResponseRedirect('/tasks/')

    return HttpResponseRedirect('/tasks/{task_id}'.format(task_id=task_id))


@tm_main_decorator
def remove_tasks_user(request, task_id, subuser_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    if str(user_id) != subuser_id:
        CoreInterface.subuser_remove(user_id, task_id, subuser_id)

    return HttpResponseRedirect('/tasks/{task_id}'.format(task_id=task_id))


@tm_main_decorator
def subtask_add(request, ownertask_id):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    CoreInterface.task_add(user_id, user_id, 'NewSubtask_T', 'Description_T')

    tasks_info = CoreInterface.get_tasks_info(user_id)
    task_info = None
    for task in tasks_info:
        if task['name'] == 'NewSubtask_T' and task['description'] == 'Description_T':
            task_info = task
            break
    CoreInterface.task_edit(user_id, task_info['id'], input_name='', input_description='')
    CoreInterface.subtask_add(user_id, ownertask_id, task_info['id'])

    return HttpResponseRedirect('/tasks/{task_id}'.format(task_id=task_info['id']))


@tm_main_decorator
def status_change(request, task_id, status_name):
    user_id = CoreInterface.get_user_id(auth.get_user(request).get_username())
    CoreInterface.status_change(user_id, task_id, status_name)

    return HttpResponseRedirect('/tasks/')


@tm_main_decorator
def show_users(request):
    if auth.get_user(request).get_username() == '':
        return HttpResponseRedirect('/')
    users_info = CoreInterface.get_users_info()
    context = {'users_info': users_info}
    return render(request, 'tm_main/user/show_users.html', context)


@tm_main_decorator
def edit_myself(request):
    if request.method == 'POST':
        form = UserEditForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data.get('id')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')

            CoreInterface.user_edit(user_id, user_id, None, first_name, last_name, email)
            return HttpResponseRedirect('/')

    else:
        user_login = auth.get_user(request).get_username()
        user_info = CoreInterface.get_user_info_by_login(user_login)

        form = UserEditForm(initial={'id': user_info['id'], 'email': user_info['email'],
                                     'first_name': user_info['first_name'], 'last_name': user_info['last_name']})
    return render(request, 'tm_main/user/edit_user.html', {'form': form})


def help(request):
    return render(request, 'tm_main/help.html')


def links(request):
    return render(request, 'tm_main/links.html')
