#!/usr/bin/env python
import os
import sys
import logging
import logging.config
import tm_website.settings as config

from tm_core.control.core_interface import CoreInterface

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tm_website.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    CoreInterface.db_address = config.DB_ADDRESS
    CoreInterface.collect_garbage(0)
    execute_from_command_line(sys.argv)


def set_loggers():
    logging_dict = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters':
    {
        'simpleFormatter':
        {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },

    'handlers':
    {
        'consoleHandler':
        {
            'class': 'logging.StreamHandler',
            'formatter': 'simpleFormatter',
            'stream': 'ext://sys.stdout'
        },
        'fileHandler':
        {
            'class': 'logging.FileHandler',
            'formatter': 'simpleFormatter',
            'filename': config.LOGFILE_PATH,
        }
    },

    'loggers':
    {
        'webLogger':
        {
            'handlers': ['fileHandler'],
            'level': config.WEB_LOGGER_LEVEL
        },
        'coreLogger':
        {
            'handlers': ['fileHandler'],
            'level': config.CORE_LOGGER_LEVEL
        }
    }}
    logging.config.dictConfig(logging_dict)
